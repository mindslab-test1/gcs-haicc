[maum.ai.haicc project]

1. 개발환경
- 스프링부트 - 2.1.2.RELEASE
- 스프링시큐리티 - 2.1.2.RELEASE
- 자바 1.8
- 그래들 - 5.2.1
- mybatis - 3.0
- mysql - mysql:mysql-connector-java:5.1.47
- thymeleaf - 2.3.0
- lombok - 1.18.4
- logback - 1.1.7

2. 프로퍼티작업
Run configurations
-VM options : -Dspring.profiles.active=dev
application.yml
spring:
  profiles:
    active: #{systemProperties['spring.profiles.active']}