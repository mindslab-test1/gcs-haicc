package maum.ai.haicc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HaiccApplication {

    public static void main(String[] args) {
        SpringApplication.run(HaiccApplication.class, args);
    }

}

