package maum.ai.haicc.mapper.user;

import maum.ai.haicc.domain.user.UserDto;
import maum.ai.haicc.domain.user.UserForm;
import maum.ai.haicc.domain.user.UserPrincipal;
import maum.ai.haicc.entity.user.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 매핑파일에 기재된 sql을 호출하기 위한 interface
 *
 * @author unongko
 * @version 1.0
 * @see /user.xml
 */

@Component
@Mapper
public interface UserMapper {
    UserPrincipal getRoleInfo(@Param("AUTHORITY") String AUTHORITY);
    UserPrincipal findUserByLoginId(@Param("loginId") String loginId);
    int setUserInfo(@Param("param") User param);
    void setUserRoleInfo(@Param("param") UserPrincipal param);

    /** 로그인 사용자 상세 조회 */
    User getLoginUserDetail(@Param("user_id") String userId);
    int updateUserEnabled(User user);
    int updateUserFailCnt(User user);

    /** 사용자 계정 - 목록 수 */
    int getUserCnt(UserForm userForm);
    /** 사용자 계정 - 목록 조회 */
    List<UserDto> getUserList(UserForm userForm);
    /** 사용자 프로필 - 비밀번호 변경 */
    int updateUserPwd(User user);
    /** 사용자 상세 조회 */
    UserDto getUserDetail(Integer user_no);
    /** 사용자 - 정보 변경 */
    int updateUserInfo(User user);
    /** 사용자 id 중복 조회 */
    UserDto getUserId(String user_id);
    /** 사용자 - 등록 */
    int insertUser(UserForm userForm);
    /** 사용자 - 단건삭제 */
    int deleteUser(UserForm userForm);
}