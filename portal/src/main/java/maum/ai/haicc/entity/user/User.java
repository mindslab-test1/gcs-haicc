package maum.ai.haicc.entity.user;

import lombok.Getter;
import lombok.Setter;
import maum.ai.haicc.domain.common.CommonVo;

/**
 * 사용자 DB 모델 (tb_user)
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class User extends CommonVo{
	private Integer user_no;
	private String user_id;
	private String user_pw;
	private String user_nm;
	private String phone_no;
	private String auth_kind;
	private String type_kind;
	private String access_menu;
	private String use_yn;
	private Integer privacy_agree;
	private String privacy_dt;
	private Integer enabled;
	private String authority;
	private Integer login_fail_cnt;
}