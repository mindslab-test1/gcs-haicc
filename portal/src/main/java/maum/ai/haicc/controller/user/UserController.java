package maum.ai.haicc.controller.user;

import com.google.gson.Gson;
import maum.ai.haicc.domain.common.ResultDto;
import maum.ai.haicc.domain.user.UserDto;
import maum.ai.haicc.domain.user.UserForm;
import maum.ai.haicc.entity.user.User;
import maum.ai.haicc.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 사용자 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class UserController<UserFrom> {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping("/admin/userManagement")
    public String userManagement(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("userManagement()");

        return "user/userManagement";
    }

    /** 사용자 - 목록 조회 */
    @RequestMapping(value = "/admin/getUserList")
    @ResponseBody
    public ResultDto getUserList(HttpServletRequest request, HttpServletResponse response, UserForm userForm) throws Exception {
        logger.info("getUserList()");
        Gson gson = new Gson();

        logger.info("{} .getUserList()", gson.toJson(userForm));
        System.out.println("getUserList() ======= " + gson.toJson(userForm));

        ResultDto resultDto = userService.getUserList(userForm);

        return resultDto;
    }

    /** 사용자 - 비밀번호 수정 */
    @RequestMapping(value = "/user/updateUserPwd")
    @ResponseBody
    public UserDto updateUserPwd(HttpServletRequest request, HttpServletResponse response, User user) throws Exception {

        UserDto userDto = new UserDto();

        Gson gson = new Gson();

        logger.info("{} .updateUser()", gson.toJson(user));

        userDto = userService.updateUserPwd(user);

        return userDto;
    }

    /** 사용자 상세 조회 */
    @RequestMapping(value = "/admin/getUserDetail")
    @ResponseBody
    public UserDto getUserDetail(HttpServletRequest request, HttpServletResponse response, UserForm userForm) throws Exception {

        UserDto userDto = new UserDto();

        Gson gson = new Gson();

        logger.info("{} .getUserDetail()", gson.toJson(userForm));

        userDto = userService.getUserDetail(userForm.getUser_no());

        return userDto;
    }

    /** 사용자 - 정보 수정 */
    @RequestMapping(value = "/admin/updateUserInfo")
    @ResponseBody
    public UserDto updateUserInfo(HttpServletRequest request, HttpServletResponse response, User user) throws Exception {

        UserDto userDto = new UserDto();

        Gson gson = new Gson();

        logger.info("{} .updateUserInfo()", gson.toJson(user));

        userDto = userService.updateUserInfo(user);

        return userDto;
    }

    /** 사용자 id 조회 */
    @RequestMapping(value = "/admin/getUserId")
    @ResponseBody
    public UserDto getUserId(HttpServletRequest request, HttpServletResponse response, User user) throws Exception {

        UserDto userDto = new UserDto();

        Gson gson = new Gson();

        logger.info("{} .getUserId()", gson.toJson(user));

        userDto = userService.getUserId(user.getUser_id());

        return userDto;
    }

    /** 사용자 - 등록 */
    @RequestMapping(value = "/admin/insertUser")
    @ResponseBody
    public UserDto insertUser(HttpServletRequest request, HttpServletResponse response, UserForm userForm) throws Exception {

        Gson gson = new Gson();

        logger.info("{} .insertUser()", gson.toJson(userForm));
        System.out.println("insertUser() ======= " + gson.toJson(userForm));

        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            User user = (User) httpSession.getAttribute("accessUser");
            if(user != null) {
                String insUserId = user.getUser_id();
                userForm.setIns_user_id(insUserId);
            }
        }

        UserDto userDto = userService.insertUser(userForm);

        return userDto;
    }

    /** 사용자 - 다건 삭제 */
    @RequestMapping(value = "/admin/deleteMultiUser")
    @ResponseBody
    public UserDto deleteMultiUser(HttpServletRequest request, HttpServletResponse response, UserForm userForm) throws Exception {

        UserDto userDto = new UserDto();
        Gson gson = new Gson();
        logger.info("{} .deleteMultiUser()", gson.toJson(userForm));
        userDto = userService.deleteMultiUser(userForm);
        return userDto;
    }
}