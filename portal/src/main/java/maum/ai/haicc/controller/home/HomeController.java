package maum.ai.haicc.controller.home;

import lombok.extern.slf4j.Slf4j;
import maum.ai.haicc.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 메인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Slf4j
@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @GetMapping("/home")
    public String home(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "home()");

        return "redirect:/hybrid/in-bound";
    }

    @GetMapping(value = {"/ai/in-bound"})
    public ModelAndView getAIInboundPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "getAIInboundPage()");

        ModelAndView mv = new ModelAndView();
        mv.addObject("menuName", "/ai/in-bound");
        mv.addObject("title", "AI InBound");
        mv.setViewName("home/content");

        return mv;
    }

    @GetMapping(value = {"/ai/out-bound"})
    public ModelAndView getAIOutboundPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "getAIOutboundPage()");

        ModelAndView mv = new ModelAndView();
        mv.addObject("menuName", "/ai/out-bound");
        mv.addObject("title", "AI OutBound");
        mv.setViewName("home/content");

        return mv;
    }

    @GetMapping(value = {"/hybrid/in-bound"})
    public ModelAndView getHybridInboundPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "getHybridInboundPage()");

        ModelAndView mv = new ModelAndView();
        mv.addObject("menuName", "/hybrid/in-bound");
        mv.addObject("title", "Hybrid InBound");
        mv.addObject("headerVisible", "N");
        mv.setViewName("home/content");

        return mv;
    }

    @GetMapping(value = {"/hybrid/in-bound/statics-main"})
    public ModelAndView getHybridInboundStaticsMainPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "getHybridInboundStaticsMainPage()");

        ModelAndView mv = new ModelAndView();
        mv.addObject("menuName", "/hybrid/in-bound/statics-main");
        mv.addObject("title", "Hybrid InBound Statics");
        mv.addObject("headerVisible", "N");
        mv.setViewName("home/content");

        return mv;
    }

    @GetMapping(value = {"/hybrid/out-bound"})
    public ModelAndView getHybridOutboundPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "getHybridOutboundPage()");

        ModelAndView mv = new ModelAndView();
        mv.addObject("menuName", "/hybrid/out-bound");
        mv.addObject("title", "Hybrid OutBound");
        mv.addObject("headerVisible", "N");
        mv.setViewName("home/content");

        return mv;
    }

    @GetMapping(value = {"/human/chatting-counseling"})
    public ModelAndView getHumanChattingCounselingPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "getHumanChattingCounselingPage()");

        ModelAndView mv = new ModelAndView();
        mv.addObject("menuName", "/human/chatting-counseling");
        mv.addObject("title", "HICC");
        mv.setViewName("home/content");

        return mv;
    }

    @GetMapping(value = {"/aiIVR"})
    public ModelAndView getAiIVRPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "getAiIVRPage()");

        ModelAndView mv = new ModelAndView();
        mv.addObject("menuName", "/aiIVR");
        mv.addObject("title", "AI-IVR");
        mv.setViewName("home/content");

        return mv;
    }

    @GetMapping(value = {"/analytics"})
    public ModelAndView getAnalyticsPage(HttpServletRequest request, HttpServletResponse response, Model model) {
        log.info("HomeController.{}", "getAnalyticsPage()");

        ModelAndView mv = new ModelAndView();
        mv.addObject("menuName", "/analytics");
        mv.addObject("title", "TMQA");
        mv.setViewName("home/content");

        return mv;
    }

}