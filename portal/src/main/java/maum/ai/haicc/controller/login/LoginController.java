package maum.ai.haicc.controller.login;

import maum.ai.haicc.domain.user.UserPrincipal;
import maum.ai.haicc.entity.user.User;
import maum.ai.haicc.service.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 로그인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @GetMapping(value = {"/", "login"})
    public ModelAndView getLoginPage(HttpServletRequest request) {
        logger.info("UserController.{}", "getLoginPage()");
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("errormsg",request.getParameter("errormsg"));
        modelAndView.setViewName("login/login");
        return modelAndView;
    }

    @GetMapping(value = {"/", "accessDenied"})
    public ModelAndView getAccessDeniedPage() {
        logger.info("UserController.{}", "getAccessDeniedPage()");
        ModelAndView mv = new ModelAndView();

        mv.setViewName("login/access-denied");

        return mv;
    }

}