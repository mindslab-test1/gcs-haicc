package maum.ai.haicc.controller.home;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@RequestMapping("/chatbot-admin")
@Controller
public class ChatBotAdminController {

  @GetMapping(value = {"/dashboard"})
  public ModelAndView getChatBotAdminDashboardPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminDashboardPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/dashboard");
    mv.addObject("title", "서비스 현황");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/chatbot-management"})
  public ModelAndView getChatBotAdminChatbotManagementPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminChatbotManagementPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/chatbot-management");
    mv.addObject("title", "챗봇 관리");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/bqa"})
  public ModelAndView getChatBotAdminBQAPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminBQAPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/bqa");
    mv.addObject("title", "BQA");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/scenario"})
  public ModelAndView getChatBotAdminScenarioPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminScenarioPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/scenario");
    mv.addObject("title", "시나리오");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/api"})
  public ModelAndView getChatBotAdminApiPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminApiPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/api");
    mv.addObject("title", "API");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/nlp-test"})
  public ModelAndView getChatBotAdmiNLPTestPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdmiNLPTestPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/nlp-test");
    mv.addObject("title", "NLP 테스트");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/workspace"})
  public ModelAndView getChatBotAdminWorkspacePage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminWorkspacePage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/workspace");
    mv.addObject("title", "Workspace 관리");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/team"})
  public ModelAndView getChatBotAdminTeamPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminTeamPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/team");
    mv.addObject("title", "Team 관리");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/user"})
  public ModelAndView getChatBotAdminUserPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminUserPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/user");
    mv.addObject("title", "User 관리");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/code"})
  public ModelAndView getChatBotAdminCodePage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminCodePage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/code");
    mv.addObject("title", "코드 관리");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/code-group"})
  public ModelAndView getChatBotAdminCodeGroupPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminCodeGroupPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/code-group");
    mv.addObject("title", "코드 그룹 관리");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/logs"})
  public ModelAndView getChatBotAdminLogsPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminLogsPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/logs");
    mv.addObject("title", "대화 이력 조회");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/error"})
  public ModelAndView getChatBotAdminErrorPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminErrorPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/error");
    mv.addObject("title", "답변불가 대화현황 조회");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/profile"})
  public ModelAndView getChatBotAdminProfilePage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminProfilePage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/profile");
    mv.addObject("title", "프로필");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/nlp-synonym"})
  public ModelAndView getChatBotAdminNlpSynonymPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminNlpSynonymPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/nlp-synonym");
    mv.addObject("title", "유의어 처리");
    mv.setViewName("home/content");

    return mv;
  }

  @GetMapping(value = {"/nlp-employees"})
  public ModelAndView getChatBotAdminNlpEmployeesPage(HttpServletRequest request, HttpServletResponse response, Model model) {
    log.info("ChatBotAdminController.{}", "getChatBotAdminNlpEmployeesPage()");

    ModelAndView mv = new ModelAndView();
    mv.addObject("menuName", "/chatbot-admin/nlp-employees");
    mv.addObject("title", "사용자 사전");
    mv.setViewName("home/content");

    return mv;
  }


}
