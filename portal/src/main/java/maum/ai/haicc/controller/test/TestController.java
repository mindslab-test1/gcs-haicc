package maum.ai.haicc.controller.test;

import maum.ai.haicc.config.PropertyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    PropertyConfig propertyConfig;

    @RequestMapping("/")
    public String test() {

        String prof =  System.getProperty("spring.profiles.active");

        String returnStr = "Server : " + prof;

        returnStr = returnStr + " : "+ propertyConfig.getObj().getNumber() + " / " + propertyConfig.getObj().getText();

        return returnStr;
    }
}
