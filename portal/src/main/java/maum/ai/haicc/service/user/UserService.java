package maum.ai.haicc.service.user;

import com.google.gson.Gson;
import maum.ai.haicc.domain.common.CommonDto;
import maum.ai.haicc.domain.common.CommonForm;
import maum.ai.haicc.domain.common.ResultDto;
import maum.ai.haicc.domain.user.UserDto;
import maum.ai.haicc.domain.user.UserForm;
import maum.ai.haicc.domain.user.UserPrincipal;
import maum.ai.haicc.entity.user.User;
import maum.ai.haicc.mapper.user.UserMapper;
import maum.ai.haicc.util.PagingUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 사용자 비지니스로직 서비스
 *
 * @author unongko
 * @version 1.0
 */

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserMapper userMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserPrincipal findUserByLoginId(String loginId) {
		return userMapper.findUserByLoginId(loginId);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserPrincipal userPrincipal = userMapper.findUserByLoginId(username);
		return userPrincipal;
	}

	public User getLoginUserDetail(String userId) throws Exception {

		User user = new User();

		user = userMapper.getLoginUserDetail(userId);

		return user;
	}

	public UserDto updateUserEnabled(User user) throws Exception {

		UserDto userDto = new UserDto();

		int updateCnt = userMapper.updateUserEnabled(user);

		if (updateCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}

	public UserDto updateUserFailCnt(User user) throws Exception {

		UserDto userDto = new UserDto();

		int updateCnt = userMapper.updateUserFailCnt(user);

		if (updateCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}

	/** 계정 - 목록 조회 */
	public ResultDto getUserList(UserForm userForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		CommonDto commonDto = new CommonDto();

		int totalCount = userMapper.getUserCnt(userForm);
		if (totalCount != 0) {
			CommonForm commonForm = new CommonForm();
			commonForm.setFunction_name(userForm.getFunction_name());
			commonForm.setCurrent_page_no(userForm.getCurrent_page_no());
			commonForm.setCount_per_page(10);
			commonForm.setCount_per_list(userForm.getCount_per_list());
			commonForm.setTatal_list_count(totalCount);
			commonDto = PagingUtil.setPageUtil(commonForm);
		}

		userForm.setLimit(commonDto.getLimit());
		userForm.setOffset(commonDto.getOffset());

		List<UserDto> list = userMapper.getUserList(userForm);

		Gson gson = new Gson();
		System.out.println("UserDto list ======= " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		resultMap.put("totalCount", totalCount);
		resultMap.put("pagination", commonDto.getPagination());

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	public UserDto updateUserPwd(User user) throws Exception {

		UserDto userDto = new UserDto();

		if(!"".contentEquals(user.getUser_pw())) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			user.setUser_pw(passwordEncoder.encode(user.getUser_pw()));
		}

		int updateCnt = userMapper.updateUserPwd(user);

		if (updateCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}

	public UserDto getUserDetail(Integer user_no) throws Exception {

		UserDto userDto = new UserDto();

		userDto = userMapper.getUserDetail(user_no);

		return userDto;
	}

	public UserDto updateUserInfo(User user) throws Exception {

		UserDto userDto = new UserDto();

		if(!"".contentEquals(user.getUser_pw())) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			user.setUser_pw(passwordEncoder.encode(user.getUser_pw()));
		}

		int updateCnt = userMapper.updateUserInfo(user);

		if (updateCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}

	public UserDto getUserId(String user_id) throws Exception {

		UserDto userDto = new UserDto();

		userDto = userMapper.getUserId(user_id);

		return userDto;
	}

	/** 사용자 - 등록 */
	public UserDto insertUser(UserForm userForm) throws Exception {

		UserDto userDto = new UserDto();

		int insertCnt = 0;

		userForm.setAuthority("ROLE_USER");

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		userForm.setUser_pw(passwordEncoder.encode(userForm.getUser_pw()));

		insertCnt = userMapper.insertUser(userForm);

		if (insertCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}

	/** 사용자 - 단건 삭제 */
	public UserDto deleteMultiUser(UserForm userForm) throws Exception {

		UserDto userDto = new UserDto();
		Gson gson = new Gson();
		System.out.println("UserService.deleteMultiUser() ======= " + gson.toJson(userForm));
		System.out.println("userForm.getUserNoArr().length ======= " + userForm.getUser_no_arr().length);

		int deleteCnt = 0;

		if(userForm.getUser_no_arr() != null) {
			for(int i=0; i < userForm.getUser_no_arr().length; i++) {
				userForm.setUser_no(userForm.getUser_no_arr()[i]);
				deleteCnt = userMapper.deleteUser(userForm);
			}
		}

		if (deleteCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}
}
