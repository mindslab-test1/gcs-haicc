package maum.ai.haicc.service.user;

import com.google.gson.Gson;
import maum.ai.haicc.domain.user.UserPrincipal;
import maum.ai.haicc.entity.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * 스프링시큐리티 사용자 권한 및 인증 처리를 위한 Provider
 *
 * @author unongko
 * @version 1.0
 */


//@Component("authProvider")
public class UserAuthenticationProvider extends DaoAuthenticationProvider {
    
    @Autowired
    private UserService userService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        User user = new User();
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        System.out.println("UserAuthenticationProvider() username ======= " + username);
        System.out.println("UserAuthenticationProvider() password ======= " + password);

        UserPrincipal userPrincipal = new UserPrincipal();

        if(!"".equals(username) && username != null){
            userPrincipal = (UserPrincipal) userService.loadUserByUsername(username);
        }else{
            throw new UsernameNotFoundException(username);
        }


        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        user.setUser_id(username);
        //비밀번호 확인
        if(!passwordEncoder.matches(password, userPrincipal.getPassword())) {
            try{
                System.out.println("UserAuthenticationProvider() userPrincipal.getLoginFailCnt() ======= " + userPrincipal.getLoginFailCnt());
                //계정상태 잠금
                if(userPrincipal.isEnabled() && userPrincipal.getLoginFailCnt() >= 4) {
                    System.out.println("UserAuthenticationProvider()updateMemberEnabled ======= ");
                    userService.updateUserEnabled(user);
                }
                //로그인실패횟수 증가
                if(userPrincipal.isEnabled() && userPrincipal.getLoginFailCnt() < 5){
                    user.setLogin_fail_cnt(userPrincipal.getLoginFailCnt()+1);
                    userService.updateUserFailCnt(user);
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            //계정상태 확인
            if(!userPrincipal.isEnabled()) {
                System.out.println("UserAuthenticationProvider() LockedException ======= ");
                throw new LockedException(username);
            }else{
                throw new BadCredentialsException(username);
            }
        }

        //계정상태 확인
        if(!userPrincipal.isEnabled()) {
            System.out.println("UserAuthenticationProvider() LockedException ======= ");
            throw new LockedException(username);
        }

        return new UsernamePasswordAuthenticationToken(username, password, userPrincipal.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
 
}