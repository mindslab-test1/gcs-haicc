package maum.ai.haicc.domain.user;

import maum.ai.haicc.entity.user.User;

/**
 * 사용자 DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

public class UserDto extends User {

	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
