package maum.ai.haicc.domain.user;

import lombok.Getter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 스프링시큐리티 로그인시 사용하는 사용자정보 모델
 *
 * @author unongko
 * @version 1.0
 */

@ToString
@Getter
public class UserPrincipal implements UserDetails {

    private String id;
    private String password;
    private String AUTHORITY;
    private boolean ENABLED;
    private String NAME;
    private Integer loginFailCnt;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> auth = new ArrayList<GrantedAuthority>();
        auth.add(new SimpleGrantedAuthority(AUTHORITY));
        return auth;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return id;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return ENABLED;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String name) {
    }

}
