package maum.ai.haicc.domain.common;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 공통 DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

@Data
@EqualsAndHashCode(callSuper=false)
public class CommonDto extends CommonVo {

	private Integer limit;
	private Integer offset;
	private String pagination;
	private Integer num;
}
