package maum.ai.haicc.domain.common;

import lombok.Data;

/**
 * 화면에서 사용하는 공통 Form 모델
 *
 * @author unongko
 * @version 1.0
 */

@Data
public class CommonForm {

	private String function_name;
	private Integer current_page_no;
	private Integer count_per_page;
	private Integer count_per_list;
	private Integer tatal_page_count;
	private Integer tatal_list_count;
	private Integer limit;
	private Integer offset;
	private String ins_user_id     = "";	// 생성자 ID
	private String ins_dt         = "";	// 생성일
	private String upd_user_id     = "";	// 수정자 ID
	private String upd_dt         = "";	// 수정일
}
