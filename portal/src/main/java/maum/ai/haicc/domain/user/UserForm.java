package maum.ai.haicc.domain.user;

import lombok.Getter;
import lombok.Setter;
import maum.ai.haicc.domain.common.CommonForm;

/**
 * 화면에서 사용하는 사용자 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class UserForm extends CommonForm {

	private Integer user_no;
	private String user_id;
	private String user_pw;
	private String user_nm;
	private String phone_no;
	private String auth_kind;
	private String type_kind;
	private String access_menu;
	private String use_yn;
	private Integer privacy_agree;
	private String privacy_dt;
	private Integer enabled;
	private String authority;
	private Integer login_fail_cnt;
	
	private String search_type;
	private String search_keyword;

	private Integer user_no_arr[];
}
