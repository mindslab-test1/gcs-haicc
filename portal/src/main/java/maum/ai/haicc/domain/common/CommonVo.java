package maum.ai.haicc.domain.common;

import lombok.Data;

/**
 * 데이터베이스에서 사용하는 공통 모델
 *
 * @author unongko
 * @version 1.0
 */

@Data
public class CommonVo {
	private Integer num;
	private String ins_user_id     = "";	// 생성자 ID
	private String ins_dt         = "";	// 생성일
	private String upd_user_id     = "";	// 수정자 ID
	private String upd_dt         = "";	// 수정일
}
