package maum.ai.haicc.handler.user;

import com.google.gson.Gson;
import maum.ai.haicc.domain.user.UserDto;
import maum.ai.haicc.domain.user.UserPrincipal;
import maum.ai.haicc.entity.user.User;
import maum.ai.haicc.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 스프링시큐리티 로그인 성공 처리 핸들러
 *
 * @author unongko
 * @version 1.0
 */

@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private UserService userService;
	
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		Gson gson = new Gson();
		System.out.println("LoginSuccessHandler auth ======= " + gson.toJson(auth));

		String userId = auth.getName();
		System.out.println("LoginSuccessHandler session userId :::::: "+ userId);

		User user = new User();
		try{
			user = userService.getLoginUserDetail(userId);
		}catch (Exception e){
			e.printStackTrace();
		}

		System.out.println("LoginSuccessHandler user ======= " + gson.toJson(user));

		if(user != null){
			HttpSession session = request.getSession();
			session.setAttribute("accessUser", user);

			try{
				user.setLogin_fail_cnt(0);
				userService.updateUserFailCnt(user);
			}catch (Exception e){
				e.printStackTrace();
			}
		}		
		
		response.sendRedirect(request.getContextPath() + "/home");
	}
}