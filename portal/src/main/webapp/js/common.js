// MINDsLab. YMJ. 20190830

$(document).ready(function (){
	//pnb
	$('.pnb').on('mouseover',function(){	
        $(this).addClass('active');
        $('#wrap').addClass('nav_show');
        $('.nav_pnb').addClass('active');
	}); 
    
	$('.pnb').on('mouseleave',function(){	
        $(this).removeClass('active');
        $('.nav_pnb').removeClass('active');
        $(this).closest('#wrap').removeClass('nav_show');	
	}); 
    
	$('.pnb ul.third').each(function(){
		$(this).parent('li').addClass('arrow'); //Show first tab content
	});
    
	$('.nav_pnb li ul.sub li>a').on('click',function(){	
        $(this).parent('li').toggleClass('selected');    
    });	 
    
    //Layer popup open 
	$('.btn_lyr_open').on('click',function(){	
        $('.lyrWrap').show();    
    });	
    
    //Layer popup close 
    $('.btn_lyr_close, .lyr_bg').on('click',function(){
        $('.lyrWrap').hide(); 
        $('.lyrBox').hide(); 
    });	
    
	$('.tab_pnb').each(function(){
        $('.tab_pnb_contents').hide();
        $('.tab_pnb_contents:first').show();

        $('.tab_pnb li a').click(function () {
            $('.tab_pnb li a').removeClass('active');
            $(this).addClass('active');
            $('.tab_pnb_contents').hide();
            
            var activeTab = $(this).attr('href');
            $(activeTab).show();
        });
	});

    
    //사용자 프로필
	$('.ico_profile.btn_lyr_open').on('click',function(){	
        $('.lyr_profile').show();    
    });	 
    
    // select
    $('.selectbox select').on('change',function(){
        var select_name = $(this).children('option:selected').text();
        $(this).siblings('label').text(select_name);
    });
    
    //Table all checkBox
	$('.ipt_tbl_allCheck').on('click',function(){	
        var iptTblAllCheck = $(this).is(":checked");
        if ( iptTblAllCheck ) {
            $(this).prop('checked', true); 
            $(this).parents('table').find('tbody td input:checkbox').prop('checked', true); 
        } else {
            $(this).prop('checked', false); 
            $(this).parents('table').find('tbody td input:checkbox').prop('checked', false); 
        }         
	});
    
    //checkBox checking
	$('.ipt_check').on('click',function(){	
        var iptChecking = $(this).is(":checked");
        if ( iptChecking ) {
            $(this).parents('.checking').find('.checkBox input:checkbox').prop('checked', false);
            $(this).prop('checked', true);  
        }
	});
        
    //상담사 메인화면(aside)
	$('.call_aside').each(function(){
		$('.call_aside .aside_container .adide_content').hide(); //Show first tab content
	
		//TAB On Click Event
		$('.nav_call_aside li button').on('click', function(){
            var callAsideIndex = $(this).parent('li').index()+1;
            
            $('.call_aside .aside_container .adide_content').hide();
            $('.nav_call_aside li button').removeClass('active');
            $('.call_aside').addClass('active');            
            $(this).addClass('active');
            $('.aside_container').find('.adide_content:nth-child('+ callAsideIndex +')').show();
            
			return false;
		});
        
		$('.call_aside .btn_adide_close').on('click', function(){
            $('.call_aside .aside_container .adide_content').hide();
            $('.nav_call_aside li button').removeClass('active');
            $('.call_aside').removeClass('active');  
		});
	});
    
	$('.selectBox').each(function(){
		var selectIndex = $(this).children('.select').length;
        
        if ( selectIndex == 1 ){
            $(this).children('.select').css({
                display:'block',
                width:'100%',
            });
        } else if ( selectIndex == 2 ){
            $(this).children('.select').css({
                width:'50%',
            });
        } else if ( selectIndex == 3 ){
            $(this).children('.select').css({
                width:'32%',
            });
        } 
	});	
    
});	

//Table scroll
var $table = $('table.scroll'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).innerWidth();
    }).get();

    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize(); // Trigger resize handler