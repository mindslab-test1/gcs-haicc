<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
				<div class="paging_wrap">
				
					<div class="paging">
						<span class="select_type single">
							<select id="pageInitPerPage3" name="pageInitPerPage3" onchange="goPage(0);">
								<option value="10" selected>10</option>
								<option value="20">20</option>
								<option value="30">30</option>
							</select>
						</span>
						
						
						<a href="javascript:detail_goPage('1')" class="paging_first"><i class="fas fa-step-backward"></i><span class="hide">처음 페이지로 이동</span></a>
						<a href="javascript:detail_goPage('${paging.prevPage}')" class="paging_prev"><i class="fas fa-caret-left"></i><span class="hide">이전 페이지로 이동</span></a>
						
						<span class="list">
						
					<c:forEach begin="${paging3.pageRangeStart}" end="${paging3.pageRangeEnd}" varStatus="loopIdx">
							
						<c:choose>
							<c:when test="${paging3.currentPage eq loopIdx.index}">
								<strong><span class="hide">현재페이지</span>${loopIdx.index}</strong>
							</c:when>
							<c:otherwise>
								<a href="javascript:detail_goPage('${loopIdx.index}')">${loopIdx.index}</a>
							</c:otherwise>
						</c:choose>

					</c:forEach>
					
						</span>
						
						<a href="javascript:detail_goPage('${paging.nextPage}')" class="paging_next"><i class="fas fa-caret-right"></i><span class="hide">다음 페이지로 이동</span></a>
						<a href="javascript:detail_goPage('${paging.totalPage}')" class="paging_last"><i class="fas fa-step-forward"></i><span class="hide">마지막 페이지로 이동</span></a>
						
					</div>
					
				</div>






