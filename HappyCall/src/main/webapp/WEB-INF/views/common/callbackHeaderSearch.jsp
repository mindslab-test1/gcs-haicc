<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="search_box">
    <ul>
        <li>
            <div class="select_type">
                <label for="schTopCampaignId">계약 종류</label>
                <select id="schTopCampaignId">
                    <option value="">계약 종류</option>
                    ${prod_name}
                </select>
            </div>
        </li>
        <li><input type="text" id="schTopCustNm" placeholder="고객명" title="고객명" onkeydown="enterCheck(this)"></li>
        <li><input type="tel" id="schTopCustTelNo" placeholder="전화번호" title="전화번호"  onKeyup="checkKey('custTelNo')" onkeydown="enterCheck(this)"></li>
        <li>
            <div class="select_type">
                <label for="schTopCallbackStatus">콜백대상여부</label>
                <select id="schTopCallbackStatus">
                    <option value="">전체</option>
                    ${callback_status}
                </select>
            </div>
        </li>
        <li>
            <div class="dateBox02">
                <input autocomplete="off" type="text" id="schTopCallbackDt" placeholder="콜백일자" title="콜백일자" class="ipt_txt" onKeyup="checkKey('callbackDt')" onkeydown="enterCheck(this)">
            </div>
        </li>
    </ul>
    <div class="btn_result">
        <button type="button" id="doSearch" class="btn_default01" onclick="goSearch(true)">결과보기</button>
    </div>
</div>
