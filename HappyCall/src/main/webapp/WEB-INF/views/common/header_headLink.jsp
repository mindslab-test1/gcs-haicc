<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- icon_favicon -->
<link rel="apple-touch-icon-precomposed" href="/resources/images/ico_favicon_64x64.png">
<link rel="shortcut icon" type="image/x-icon" href="/resources/images/ico_favicon_64x64.ico">
<!-- resources -->
<link rel="stylesheet" type="text/css" href="/resources/css/font.css">
<link rel="stylesheet" type="text/css" href="/resources/css/all.css">
<link rel="stylesheet" type="text/css" href="/resources/css/datepicker.css">
<link rel="stylesheet" type="text/css" href="/resources/css/common.css">
<link rel="stylesheet" type="text/css" href="/resources/css/jquery.datetimepicker.css">
<link rel="stylesheet" type="text/css" href="/resources/css/jquery-ui.css">
<!-- //resources -->
