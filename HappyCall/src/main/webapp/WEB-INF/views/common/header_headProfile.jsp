<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!-- header -->
	<div id="header">
		<h1><a href="#none"><i class="fas fa-headphones-alt"></i> HAPPY <span>CALL</span></a></h1>
		<div class="user_wrap">
		<!-- <div class="notice"><a href="#none"><i class="fas fa-bell"></i><span class="new"><em class="hide">새로운 소식이 있습니다.</em></span></a></div> -->
			<div class="user_area">
				<div class="toggle_type01">
					<div class="user_box">
						<a href="#none" class="btn_toggle01">${username} <i class="fas fa-user-circle"></i></a>
						<div class="cont_detail">
							<ul class="list">
								<li><a href="javascript:logout();">LOG OUT</a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //header -->
