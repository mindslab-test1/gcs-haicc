<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<%@ include file="common/header_headLink.jsp" %>

<title>Happy Call</title>
</head>
<body>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
		<span></span>
		<span></span>
		<span></span>
		<span></span>
    </div>
</div>
<!-- //.page loading -->
<!-- wrap -->
<div id="wrap"> 
	<!-- header -->
	<div id="header" class="txt_center">
		<h1><a href="#none"><i class="fas fa-headphones-alt"></i> HAPPY <span>CALL</span></a></h1>
	</div>
	<!-- //header -->
	<!-- container -->
	<div id="container">
		<!-- login -->
		<div id="login">
			<div class="visual_area">
				<div class="txt_welcome">
					<strong>HAPPY CALL</strong>
					<p>상담사님 반갑습니다!</p>
				</div>
			</div>
			<div class="login_area">
			<div class="login_wrap">
					<h2>LOG IN</h2>
					
					<form id="valueForm" name="valueForm">
					
						<div class="input_area">
							<div class="input_id">
								<input type="text" id="idType" name="username" placeholder="ID" onkeypress="enterCheck(event)">
								<label for="idType" class="hide">아이디</label>
								<i class="fas fa-user"></i>
							</div>
							<div class="input_pw">
								<input type="password" id="pwType" name="password" placeholder="Password" onkeypress="enterCheck(event)">
								<label for="pwType" class="hide">비밀번호</label>
								<i class="fas fa-lock"></i>
							</div>
							<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"/>

							<div class="checkbox form_type01"><input type="checkbox" id="saveId" name="saveId" value="1" checked="checked"><label for="saveId">아이디 저장</label></div>
						</div>
						<button class="btn_login" onclick="goLogin()">Log in</button>
					</form>
					
				</div>
				<div id="footer">MINDsLab © 2019</div>
			</div>
		</div>
		<!-- //login -->
	</div>
	<!-- //container -->
</div>
<!-- //wrap -->

<!-- javascript link & init -->
<%@ include file="common/footer_init.jsp" %>
<!-- //javascript link & init -->

<script type="text/javascript">
$(document).ready(function(){
	init();
});

//initialize. 아이디저장 체크에 따른 초기값 세팅
function init(){
	var ckVal = $.cookie('saveId');
	
	if( $('#saveId').is(":checked") && ckVal.length > 0 ){
		$('#idType').val(ckVal);
		$('#pwType').focus();
	}else{
		$('#idType').focus();
	}
}

//Login
function goLogin(){
	var id = $('#idType').val();
	var pw = $('#pwType').val();

	if( id.length < 1 ){
		alert("ID를 입력하시기 바랍니다.");
		$('#idType').val();
		$('#idType').focus();
		return false;
	}
	else if( pw.length < 1 ){
		alert("비밀번호를 입력하시기 바랍니다.");
		$('#pwType').val();
		$('#pwType').focus();
		return false;
	}

	if( $('#saveId').is(":checked") ){
		$.cookie('saveId', id, {expire:7});
	}

	valueForm.method = "POST";
	valueForm.action = "/loginProcess"
	valueForm.submit();
}


//엔터키 처리
function enterCheck(event){
	if( window.event.keyCode == 13 ){
		goLogin();
	}else{
		return false;
	}
}
</script>

</body>
</html>


