﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<%@ include file="../common/header_headLink.jsp" %>
<title>Happy Call</title>
</head>
<body style="width:800px; height:600px;">
<div id="popup_wrap">
	<!-- popup_header -->
	<div class="popup_header">
		<h1>모니터링 <span>실행 결과</span></h1>
	</div>
	<!-- //popup_header -->

	<!-- popup_contents -->
	<div class="popup_contents">
		<!-- customInfo_wrap -->
		<div class="customInfo_wrap">
			<div class="custom_list">
				<table>
					<caption>고객 상세</caption>
					<colgroup>
						<col style="width:100px">
						<col style="width:auto">
					</colgroup>
					<tbody>
					<tr>
						<th scope="row">고객명</th>
						<td>${topInfo.custNm}</td>
					</tr>
					<tr>
						<th scope="row">상품명</th>
						<td>${topInfo.prodNm}</td>
					</tr>
					<tr>
						<th scope="row">주민번호</th>
						<td>${topInfo.juminNo}</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="custom_list">
				<table>
					<caption>고객 상세</caption>
					<colgroup>
						<col style="width:100px">
						<col style="width:auto">
					</colgroup>
					<tbody>
					<tr>
						<th scope="row">실행일자</th>
						<td>
							<span>${topInfo.callDate}</span>
						</td>
					</tr>
					<tr>
						<th scope="row">고객 관리 번호</th>
						<td>${topInfo.custId}</td>
					</tr>
					<tr>
						<th scope="row">모니터링 종류</th>
						<td>${topInfo.campaignNm}</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //customInfo_wrap -->
		<!-- cont_left -->
		<div class="cont_left">
			<div class="tit_wrap">
				<h2>회차별 통화 내용</h2>
				<div class="btn_area">
					<button type="button" class="btn_default01_sm" onclick="goLast('${topInfo.callTryCount}')">최종회차</button>
					<div class="select_type">
						<label for="selCtn">회차 선택</label>
						<select id="selCtn" onchange="javascript:goCtn();">
							<option value="0">회차 선택</option>
				<c:choose>
					<c:when test="${fn:length(callTriedNoList) gt 0}">
						<c:forEach items="${callTriedNoList}" var="ctn" varStatus="status">
							<option value="${ctn}">${status.count}</option>
						</c:forEach>
					</c:when>
				</c:choose>
						</select>
					</div>
				</div>
			</div>
			<div class="monitor_state"><!-- 비활성화일 때 class에 off를 추가해 주세요 -->
				<i class="fas fa-grin-beam"><span class="hide">Good</span></i>
				<i class="fas fa-grin-beam-sweat off"><span class="hide">Not Good</span></i>
				<i class="fas fa-tired off"><span class="hide">Bad</span></i>
			</div>
			<!-- call_box -->
			<div class="call_box">
				<!-- call_player -->
				 <div class="call_player">
					<audio src="http://${audioUrl}/call/record/${frontMntVO.callId}/${frontMntVO.cno}/${frontMntVO.campaignId}" preload="auto" controls></audio>
				</div>
				<!-- //call_player -->
				<!-- call_cont -->
				<div class="call_cont">
					<!-- call_list -->
					<ul id="call_list" class="call_list">
						<c:choose>
							<c:when test="${fn:length(sttResult) gt 0}">
								<c:forEach items="${sttResult}" var="sttResultList" varStatus="status">
									<c:choose>
										<c:when test="${sttResultList.speakerCode eq 'ST0002'}">
											<!-- agent -->
											<li class="agent">
												<span class="thumb"><i class="fas fa-robot"></i><span class="speaker">AI</span></span>
												<span class="cont"><em class="txt">${sttResultList.sentence}</em></span>
											</li>
											<!-- //agent -->
										</c:when>
										<c:otherwise>
											<!-- client -->
											<li class="client">
												<span class="thumb"><i class="fas fa-user"></i><span class="speaker">고객</span></span>
												<c:choose>
													<c:when test="${sttResultList.ignored eq 'Y'}">
														<span class="cont"><em class="txt_ignored">${sttResultList.sentence} - IGNORED</em></span>
													</c:when>
													<c:otherwise>
														<span class="cont"><em class="txt">${sttResultList.sentence}</em></span>
													</c:otherwise>
												</c:choose>
											</li>
											<!-- //client -->
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
	
					</ul>
					<!-- //call_list -->
				</div>
				<!-- //call_cont -->
			</div>
			<!-- //call_box -->
		</div>
		<!-- //cont_left -->
		<!-- cont_right -->
		<div class="cont_right">
			<!-- 모니터링 결과 -->
			<h2>모니터링 결과</h2>
			<div class="tbl_customTh">
				<table>
					<caption>모니터링 결과 제목</caption>
					<colgroup>
						<col style="width:40px">
						<col style="width:auto">
						<col style="width:134px">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">No.</th>
							<th scope="col">구간</th>
							<th scope="col">탐지</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="tbl_customTd">
				<table>
					<caption>모니터링 결과 내용</caption>
					<colgroup>
						<col style="width:40px">
						<col style="width:auto">
						<col style="width:128px">
					</colgroup>
					<tbody>
						<c:choose>
							<c:when test="${fn:length(mntResult) gt 0}">
								<c:forEach items="${mntResult}" var="mntResult" varStatus="status">
									<tr>
										<td class="txt_center">${status.index + 1}</td>
										<td>${mntResult.taskInfo}</td>
										<td>

											<c:choose>
												<c:when test="${score[mntResult.seq] eq 'Y'}">
													<div class="radiobox form_type01">
														<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}" checked><label for="resultYes${mntResult.seq}">Yes</label>
													</div>
													<div class="radiobox form_type01">
														<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}"><label for="resultNo${mntResult.seq}">No</label>
													</div>
												</c:when>
												<c:when test="${score[mntResult.seq] eq 'N'}">
													<div class="radiobox form_type01">
														<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}" checked><label for="resultYes${mntResult.seq}">Yes</label>
													</div>
													<div class="radiobox form_type01">
														<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}" checked><label for="resultNo${mntResult.seq}">No</label>
													</div>
												</c:when>
												<c:otherwise>
													<div class="radiobox form_type01">
														<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}"><label for="resultYes${mntResult.seq}">Yes</label>
													</div>
													<div class="radiobox form_type01">
														<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}"><label for="resultNo${mntResult.seq}">No</label>
													</div>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</c:forEach>
							</c:when>
						</c:choose>
					</tbody>
				</table>
			</div>
			<!-- //모니터링 결과 -->
			<!-- 상담 메모 -->
			<div class="textarea_wrap single02">
				<textarea id="popMemo" rows="3" cols="50" onKeyup="checkPopMemoLen()" placeholder="상담 메모 (글자수 최대 255자)" class="textarea_box">${memo}</textarea>
				<span id="popMemoLen">0/255</span>
			</div>
			<!-- //상담 메모 -->
		</div>
		<!-- //cont_right -->
	</div>
	<!-- //popup_contents -->
	<!-- popup_bottom -->
	<div class="popup_bottom">
		<div id="sign_calling" class="txt_state">
			<!-- <p class="headset"><i class="fas fa-headset"></i> 고객과 직접 통화중</p> -->
			<p class="robot"><i></i></p>
		</div>
		<div class="btnPage_wrap">
			<div style="display: flex; justify-content: space-between;">
				<button type="button" id="call_memo_save" class="btn_default01" onClick="javascript:confirmSave();">메모 저장</button>
				<button type="button" class="btn_default01" onClick="javascript:self.close();">확인</button>
			</div>

			<%--// 각각의 알림창 관련 id--%>
			<div id="successDialog" class="dialog"></div>
			<div id="failDialog" class="dialog"></div>
			<div id="saveDialog" class="dialog"></div>
			<div id="confirmDialog" class="dialog"></div>
            <div id="goCtnDialog" class="dialog"></div>

		</div>
	</div>
	<!-- //popup_bottom -->
</div>
<input type="hidden" id="cno" name="cno" value="${frontMntVO.cno}" />
<input type="hidden" id="ctn" name="ctn" value="${frontMntVO.ctn}" />
<!-- javascript link & init -->
<%@ include file="../common/footer_init_popup.jsp" %>
<!-- //javascript link & init -->

<script type="text/javascript">
$(document).ready(function(){

	init();

	$("#selCtn").val(${frontMntVO.ctn}).attr("selected","selected");

	var call_now = $("#selCtn option").index($("#selCtn option:selected"));
	var call_total = $("#selCtn option").size()-1;

	$(".popup_paging").append("전체 <strong>"+call_now+"</strong> / "+call_total);
	audioPlayerInit();
});

//실시간 메세지 관련 팝업 실행시 값 설정 및 초기화
function init(){
	var successDialogMsg = '저장 완료되었습니다.';
	var successDialogId = 'successDialog';
	var successDialogType = 'success';
	setDialog(successDialogId, successDialogMsg, successDialogType);

	var failDialogMsg = '저장 실패되었습니다.';
	var failDialogId = 'failDialog';
	var failDialogType = 'fail';
	setDialog(failDialogId, failDialogMsg, failDialogType);

	var saveDialogMsg = '상담메모는 최대 255자까지 가능합니다.';
	var saveDialogId = 'saveDialog';
	var saveDialogType = 'save';
	setDialog(saveDialogId, saveDialogMsg, saveDialogType);

	var confirmDialogMsg = '저장하시겠습니까?';
	var confirmDialogId = 'confirmDialog';
	var confirmDialogType = 'confirm';
	setDialog(confirmDialogId, confirmDialogMsg, confirmDialogType);

    var goCtnDialogMsg = '잘못된 회차 선택입니다.';
    var goCtnDialogId = 'goCtnDialog';
    var goCtnDialogType = 'goCtn';
    setDialog(goCtnDialogId, goCtnDialogMsg, goCtnDialogType);

	checkPopMemoLen();
}

function confirmSave() {
	$('#confirmDialog').dialog('open');
}

//"저장" 클릭
function doPopSave(){

	var cno = $('#cno').val();											//contract_no
	var callId = '<c:out value="${topInfo.callId}" />';				    //call_id
	var campId = '<c:out value="${topInfo.campaignId}" />';			    //campaign_id
	var popMemo = $('#popMemo').val();									//상담메모
	var checkedArr = [];												//체크된 체크박스 값
	var checkedDetect = '';												//checkedArr을 string으로 변경.

	//체크된 체크박스 확인하여 string으로 변경.
	$('input[type=radio]:checked').each(function(){
		checkedArr.push($(this).val());
	});

	if( checkedArr.length > 0 ){
		checkedDetect = checkedArr.join(',');
	}
	if( popMemo.length > 255 ) {
		$('#saveDialog').dialog('open');
		return false;
	}

	$.ajax({
		url : "/mntResultPopSave",
		data : {cno:cno, callId:callId, campaignId:campId, popMemo:popMemo, checkedDetect:checkedDetect},
		type : "POST",
		beforeSend : function(xhr) {
			/*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
			xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
		},
	}).done(function(data){
		if(data == "SUCC"){
			$("#call_end_next").attr("disabled", false);				//저장되었으므로 "다음 콜 걸기"버튼 활성화
			$('#successDialog').dialog('open');
		} else {
			$('#failsDialog').dialog('open');
		}
	}).fail(function(data){
		if(data == "FAIL"){												//저장 실패이므로 추가 액션 없음.
			$('#failsDialog').dialog('open');
		}
	});
}

//상담메모 입력 문자 길이 체크
function checkPopMemoLen() {
	var popMemo = $('#popMemo').val();
	$('#popMemoLen').html(popMemo.length + "/255");
}

function setDialog(id, msg, type) {
	var buttons;
	if(type === 'confirm') {
		buttons= [
			{
				text: "OK",
				click: function() {
					doPopSave();
					$(this).dialog("close");
				}
			},
			{
				text: "Cancel",
				click: function() {
					$(this).dialog("close");
				}
			}
		];
	} else {
		buttons = [
			{
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
				}
			}
		];
	}

	$('#'+id).html(msg);
	$('#'+id).dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: buttons
	});
}

//최종회차 버튼 클릭
function goLast(){
	$("#selCtn option:last").attr("selected","selected");
	var lastCallNo = $('#selCtn').val();

	document.location.href = "http://" + location.host + "/mntResultPop?cno=" + $('#cno').val() + "&ctn=" + lastCallNo;
}

//회차선택 셀렉트박스 변경
function goCtn(){
	var ctn = $('#selCtn').val();

	if( ctn == "0" ){
		$('#goCtnDialog').dialog('open');
		return false;
	}
	
	document.location.href = "http://" + location.host + "/mntResultPop?cno=" + $('#cno').val() + "&ctn=" + ctn;
}
function audioPlayerInit() {
	$('#audioPlayer').each(function(){
	    this.pause(); // Stop playing
	    this.currentTime = 0; // Reset time
	});
	
	try{
		$('.audioplayer-playpause').attr( 'title','play');
	    $('.audioplayer-playpause a').html( 'Play');
	    $('.audioplayer').removeClass('audioplayer-playing');
	    $('.audioplayer').addClass('audioplayer-stopped');	
	}catch(e){
		console.log('audio stop exception',e);
	}
}

</script>
</body>
</html>





