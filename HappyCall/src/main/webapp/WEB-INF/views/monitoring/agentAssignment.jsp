<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <%@ include file="../common/header_headLink.jsp" %>

</head>

<body>
<form id="valueForm" name="valueForm">

    <!-- .page loading -->
    <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}"/>
    <div id="page_ldWrap" class="page_loading">
        <div class="loading_itemBox"><span></span><span></span><span></span><span></span></div>
    </div>
    <!-- //.page loading -->

    <!-- wrap -->
    <div id="wrap">
        <!-- header -->
        <%@ include file="../common/header_headProfile.jsp" %>
        <!-- //header -->

        <!-- container -->
        <div id="container">
            <!-- snb -->
            <%@ include file="../common/leftMenu.jsp" %>
            <!-- //snb -->
            <!-- contents -->
            <div id="contents">
                <!-- search_box -->
                <div class="search_box02">
                    <ul>
                        <li>
                            <input name="check_campaign_id" type="text" placeholder="캠페인 ID" title="캠페인 ID"
                                   value="${frontMntVO.check_campaign_id }" onkeydown="enterCheck(this)">
                        </li>
                        <li>
                            <div class="select_type">
                                <label for="schTopMntType">모니터링 종류</label>
                                <select id="schTopMntType">
                                    <option value="">모니터링 종류</option>
                                    ${mntType}
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="select_type">
                                <label for="schTopOpId">상담사명</label>
                                <select id="schTopOpId">
                                    <option value="">상담사명</option>
                                    ${custInfoCode}
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="dateBox02">
                                <input autocomplete="off" type="text" id="schTopTargetDt" placeholder="대상일자" title="대상일자" class="ipt_txt" onKeyup="checkKey('targetDt')" onkeydown="enterCheck(this)">
                            </div>
                        </li>
                        <li>
                            <div class="select_type">
                                <label for="schTopAssignedYn">배정여부</label>
                                <select id="schTopAssignedYn">
                                    <option value="" selected>배정여부</option>
                                    <option value="Y">Y</option>
                                    <option value="N">N</option>
                                </select>
                            </div>
                        </li>
                        <li><input type="text" placeholder="고객명" title="고객명" id="schTopCustNm" onkeydown="enterCheck(this)"></li>
                        <li><input type="text" placeholder="고객관리번호" title="고객관리번호" id="schTopCustId" onkeydown="enterCheck(this)"></li>
                    </ul>
                    <div class="btn_result">
                        <button type="button" class="btn_default01" onclick="goSearch(true)">조회</button>
                    </div>
                </div>
                <!-- //search_box -->
                <!-- 대상 리스트 & 배정현황 -->
                <div class="div_wrap">
                    <!-- 대상 리스트 -->
                    <div class="list_wrap">
                        <h2>대상 리스트</h2>
                        <div class="data_list tbl_thead">
                            <table>
                                <colgroup>
                                    <col style="width:45px;">
                                    <col style="width:15%;">
                                    <col style="width:20%;">
                                    <col style="width:auto;">
                                    <col style="width:10%;">
                                    <col style="width:18%;">
                                    <col style="width:12%;">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="col">
                                        <div class="checkbox form_type01">
                                            <input type="checkbox" id="checkBox00"><label for="checkBox00"></label>
                                        </div>
                                    </th>
                                    <th id='CAMPAIGN_ID' scope="col" onclick="onChangeSort(this.id, 1)">캠페인 ID</th>
                                    <th id='CAMPAIGN_NM' scope="col" onclick="onChangeSort(this.id, 1)">모니터링 종류</th>
                                    <th id='CUST_ID' scope="col" onclick="onChangeSort(this.id, 1)">고객관리번호</th>
                                    <th id='CUST_NM' scope="col" onclick="onChangeSort(this.id, 1)">고객명</th>
                                    <th id='TARGET_DT' scope="col" onclick="onChangeSort(this.id, 1)">대상일자</th>
                                    <th id='ASSIGNED_YN' scope="col" onclick="onChangeSort(this.id, 1)">배정여부</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="data_list tbl_tbody half_height">
                            <table>
                                <colgroup>
                                    <col style="width:45px;">
                                    <col style="width:15%;">
                                    <col style="width:20%;">
                                    <col style="width:auto;">
                                    <col style="width:10%;">
                                    <col style="width:18%;">
                                    <col style="width:12%;">
                                </colgroup>
                                <tbody>
                                <c:forEach var="data" items="${list}" varStatus="status">
                                    <tr>
                                        <td>
                                            <div class="checkbox form_type01">
                                                <input type="checkbox" name="checkbox1" value="${data.contractNo}" id="checkBox01-${status.index}">
                                                <label for="checkBox01-${status.index}"></label>
                                            </div>
                                        </td>
                                        <td>${data.campaignId}</td>
                                        <td>${data.campaignNm }</td>
                                        <td>${data.custId }</td>
                                        <td>${data.custNm }</td>
                                        <td>${data.targetDt }</td>
                                        <td>${data.assignedYn }</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!-- page_area -->
                        <div class="page_area">
                            <%@ include file="../common/paging.jsp" %>
                        </div>
                        <!-- //page_area -->
                    </div>
                    <!-- //대상 리스트 -->
                    <div class="arrow_area">
                        <div class="align">
                            <a href="#none" class="arrow_assign" onclick="checkConfirm('random');">배정</a>
                            <a href="#none" class="arrow_choice" onclick="checkConfirm('choice');">선택 배정</a>
                        </div>
                    </div>
                    <!-- 배정현황 -->
                    <div class="assign_wrap">
                        <h2>배정현황</h2>
                        <div class="data_list tbl_thead">
                            <table>
                                <colgroup>
                                    <col style="width:45px;">
                                    <col style="width:30%;">
                                    <col style="width:30%;">
                                    <col style="width:auto;">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th id='CUST_OP_NM' scope="col" onclick="onChangeSort(this.id, 2)">상담사명</th>
                                    <th id='CUST_OP_STATUS_NM' scope="col" onclick="onChangeSort(this.id, 2)">상태</th>
                                    <th id='COUNT' scope="col" onclick="onChangeSort(this.id, 2)">배정 건수</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="data_list tbl_tbody half_height">
                            <table>
                                <colgroup>
                                    <col style="width:45px;">
                                    <col style="width:30%;">
                                    <col style="width:30%;">
                                    <col style="width:auto;">
                                </colgroup>
                                <tbody>
                                <c:forEach var="data" items="${opList}" varStatus="status">
                                    <tr onclick="check_assign_click(${data.custOpId});">
                                        <td>
                                            <div class="checkbox form_type01" onclick="event.stopPropagation()">
                                                <input type="checkbox" name="checkbox2" value="${data.custOpId}"
                                                       id="checkBox02-${status.index}"><label
                                                    for="checkBox02-${status.index}"></label>
                                            </div>
                                        </td>
                                        <td>${data.custOpNm}</td>
                                        <td>${data.custOpStatusNm}</td>
                                        <td>${data.count}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!-- page_area -->
                        <div class="page_area">
                            <%@ include file="../common/paging2.jsp" %>
                        </div>
                        <!-- //page_area -->
                    </div>
                    <!-- //배정현황 -->
                </div>
                <!-- //대상 리스트 & 배정현황 -->
                <!-- data list -->
                <h2>상담사별 상세</h2>
                <div class="data_list tbl_thead">
                    <table>
                        <colgroup>
                            <col style="width:45px;">
                            <col style="width:15%;">
                            <col style="width:10%;">
                            <col style="width:auto;">
                            <col style="width:10%;">
                            <col style="width:12%;">
                            <col style="width:12%;">
                            <col style="width:12%;">
                            <col style="width:12%;">
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">
                                <div class="checkbox form_type01">
                                    <input type="checkbox" id="checkBox00-03"><label for="checkBox00-03"></label>
                                </div>
                            </th>
                            <th scope="col">캠페인 ID</th>
                            <th scope="col">모니터링 종류</th>
                            <th scope="col">고객관리번호</th>
                            <th scope="col">고객명</th>
                            <th scope="col">대상일자</th>
                            <th scope="col">배정일자</th>
                            <th scope="col">상담사명</th>
                            <th scope="col">진행상태</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="data_list tbl_tbody half_height">
                    <table>
                        <colgroup>
                            <col style="width:45px;">
                            <col style="width:15%;">
                            <col style="width:10%;">
                            <col style="width:auto;">
                            <col style="width:10%;">
                            <col style="width:12%;">
                            <col style="width:12%;">
                            <col style="width:12%;">
                            <col style="width:12%;">
                        </colgroup>
                        <tbody>
                        <c:forEach var="data" items="${detailList}" varStatus="status">
                            <tr>
                                <td>
                                    <div class="checkbox form_type01">
                                        <input type="checkbox" name="checkbox3" value="${data.contractNo}"
                                               id="checkBox03-${status.index}"><label
                                            for="checkBox03-${status.index}"></label>
                                    </div>
                                </td>
                                <td>${data.campaignId}</td>
                                <td>${data.campaignNm}</td>
                                <td>${data.custId}</td>
                                <td>${data.custNm}</td>
                                <td>${data.targetDt}</td>
                                <td>${data.assignedDt}</td>
                                <td>${data.custOpNm}</td>
                                <td>${data.mntStatusName}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!-- //data list -->
                <!-- page_area -->
                <div class="page_area">
                    <div class="btn_monitor">
                        <button type="button" class="btn_default04" onclick="checkConfirm('cancel');">배정 취소</button>
                    </div>
                    <%@ include file="../common/paging3.jsp" %>
                </div>
                <!-- //page_area -->
            </div>
            <!-- //contents -->

            <input type="hidden" id="currentPage" name="currentPage" value="${paging.currentPage}"/>
            <input type="hidden" id="currentPage2" name="currentPage2" value="${paging2.currentPage}"/>
            <input type="hidden" id="currentPage3" name="currentPage3" value="${paging3.currentPage}"/>
            <input type="hidden" id="schOpId" name="schOpId" value="${frontMntVO.schOpId}"/>
            <input type="hidden" id="schOpIdDetail" name="schOpIdDetail"/>
            <input type="hidden" id="schCampId" name="schCampId" />
            <input type="hidden" id="schMntType" name="schMntType" />
            <input type="hidden" id="schTargetDt" name="schTargetDt" />
            <input type="hidden" id="schAssignedYn" name="schAssignedYn" />
            <input type="hidden" id="schCustNm" name="schCustNm" />
            <input type="hidden" id="schCustId" name="schCustId" />


            <input type="hidden" id="sortingTarget" name="sortingTarget" />
            <input type="hidden" id="direction" name="direction" />
            <input type="hidden" id="sortingTarget2" name="sortingTarget2" />
            <input type="hidden" id="direction2" name="direction2" />
            <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}"/>
        </div>

        <%--// 각각의 알림창 관련 id--%>
        <div id="successDialog" class="dialog"></div>
        <div id="failDialog" class="dialog"></div>
        <div id="warnDialog" class="dialog"></div>
        <div id="randomDialog" class="dialog"></div>
        <div id="choiceDialog" class="dialog"></div>
        <div id="cancelDialog" class="dialog"></div>
        <!-- //container -->
        <!-- footer -->
        <div id="footer">

        </div>
        <!-- //footer -->
    </div>
    <!-- //wrap -->

    <!-- javascript link & init -->
    <%@ include file="../common/footer_init.jsp" %>
    <!-- //javascript link & init -->

    <script type="text/javascript">
        var sortingTarget = "${frontMntVO.sortingTarget}";
        var direction = "${frontMntVO.direction}";
        var sortingTarget2 = "${frontMntVO.sortingTarget2}";
        var direction2 = "${frontMntVO.direction2}";

        $(document).ready(function () {
            var rows = '<c:out value="${frontMntVO.pageInitPerPage}" />';
            if( rows != '' ){
                $('#pageInitPerPage').val(rows);
            }
            var rows2 = '<c:out value="${frontMntVO.pageInitPerPage2}" />';
            if( rows2 != '' ){
                $('#pageInitPerPage2').val(rows2);
            }
            var rows3 = '<c:out value="${frontMntVO.pageInitPerPage3}" />';
            if( rows3 != '' ){
                $('#pageInitPerPage3').val(rows3);
            }

            $('#schTopCampId').val( '<c:out value="${frontMntVO.schCampId}" />' );
            $('#schTopMntType').val( '<c:out value="${frontMntVO.schMntType}" />' );
            $('#schTopOpId').val( '<c:out value="${frontMntVO.schOpId}" />' );
            $('#schTopTargetDt').val( '<c:out value="${frontMntVO.schTargetDt}" />' );
            $('#schTopAssignedYn').val( '<c:out value="${frontMntVO.schAssignedYn}" />' );
            $('#schTopCustNm').val( '<c:out value="${frontMntVO.schCustNm}" />' );
            $('#schTopCustId').val( '<c:out value="${frontMntVO.schCustId}" />' );

            $('#sortingTarget').val( '<c:out value="${frontMntVO.sortingTarget}" />' );
            $('#direction').val( '<c:out value="${frontMntVO.direction}" />' );
            $('#sortingTarget2').val( '<c:out value="${frontMntVO.sortingTarget2}" />' );
            $('#direction2').val( '<c:out value="${frontMntVO.direction2}" />' );

            if(sortingTarget) {
                initSort(1);
            }
            if(sortingTarget2) {
                initSort(2);
            }
        });
            $(document).ready(function () {
                var successDialogMsg = '완료되었습니다.';
                var successDialogId = 'successDialog';
                var successDialogType = 'success';
                setDialog(successDialogId, successDialogMsg, successDialogType);

                var failDialogMsg = '실패되었습니다.';
                var failDialogId = 'failDialog';
                var failDialogType = 'fail';
                setDialog(failDialogId, failDialogMsg, failDialogType);

                var warnDialogMsg = '대상을 선택해주세요.';
                var warnDialogId = 'warnDialog';
                var warnDialogType = 'warn';
                setDialog(warnDialogId, warnDialogMsg, warnDialogType);

                var randomDialogMsg = '배정 하시겠습니까?';
                var randomDialogId = 'randomDialog';
                var randomDialogType = 'random';
                setDialog(randomDialogId, randomDialogMsg, randomDialogType);

                var choiceDialogMsg = '선택 배정 하시겠습니까?';
                var choiceDialogId = 'choiceDialog';
                var choiceDialogType = 'choice';
                setDialog(choiceDialogId, choiceDialogMsg, choiceDialogType);

                var cancelDialogMsg = '배정 취소 하시겠습니까?';
                var cancelDialogId = 'cancelDialog';
                var cancelDialogType = 'cancel';
                setDialog(cancelDialogId, cancelDialogMsg, cancelDialogType);
            });

        function setDialog(id, msg, type) {
            var buttons;
            if(type === 'random') {
                buttons = [
                    {
                        text: "OK",
                        click: function() {
                            randomAssign();
                            $(this).dialog("close");
                            opener.location.reload();
                        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else if(type ==='choice') {
                buttons = [
                    {
                        text: "OK",
                        click: function() {
                            choiceAssign();
                            $(this).dialog("close");
                            opener.location.reload();
                        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else if(type ==='cancel') {
                buttons = [
                    {
                        text: "OK",
                        click: function() {
                            cancelAssign();
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else if(type ==='warn') {
                buttons = [
                    {
                        text: "Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ];
            } else {
                buttons = [
                    {
                        text: "Cancel",
                        click: function () {
                            $(this).dialog("close");
                            location.reload();
                        }
                    }
                ];
            }

            $('#'+id).html(msg);
            $('#'+id).dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                buttons: buttons
            });
        }

        function checkConfirm(flag) {
            var checkedRow1 = $('input:checkbox[name=checkbox1]:checked');
            var checkedRow2 = $('input:checkbox[name=checkbox2]:checked');
            var checkedRow3 = $('input:checkbox[name=checkbox3]:checked');

            if (flag === 'random') {
                if (checkedRow1.length == 0) {
                    $('#warnDialog').dialog('open');
                } else {
                    $('#randomDialog').dialog('open');
                }
            } else if (flag === 'choice') {
                if (checkedRow1.length == 0 && checkedRow2.length == 0) {
                    $('#warnDialog').dialog('open');
                } else {
                    $('#choiceDialog').dialog('open');
                }
            } else if (flag === 'cancel') {
                if (checkedRow3.length == 0) {
                    $('#warnDialog').dialog('open');
                } else {
                    $('#cancelDialog').dialog('open');
                }
            }
        }

        var checkout = $('#schTopTargetDt').datepicker().on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');

        //무작위 배정
       function randomAssign() {
            var contractNoList = getCheckedContractData();
            var param = {"contractNoList": contractNoList};

            jQuery.ajaxSettings.traditional = true;
            $.ajax({
                method: 'POST',
                url: '/setOpByRandom',
                data: param,
                beforeSend : function(xhr) {
                    /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
                //지정 성공시
            }).done(function(data){
                if(data === "SUCC"){
                    $('#successDialog').dialog('open');
                } else if(data === "FAIL") {
                    $('#failDialog').dialog('open');
                }
                //지정 실패시
            }).fail(function(data){
                $('#failDialog').dialog('open');
            });
        };

        //선택 배정
        function choiceAssign() {
            var custOpId = getCheckedOpData();
            var contractNoList = getCheckedContractData();
            var param = {
                "contractNoList": contractNoList,
                "custOpId": custOpId
            };

            jQuery.ajaxSettings.traditional = true;
            $.ajax({
                method: 'POST',
                url: '/setOp',
                data: param,
                beforeSend : function(xhr) {
                    /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
            }).done(function (data) {
                if(data === "SUCC"){
                    $('#successDialog').dialog('open');
                } else if(data === "FAIL") {
                    $('#failDialog').dialog('open');
                }
            }).fail(function (data) {
                $('#failDialog').dialog('open');
            });
        };

        //배정 취소
        function cancelAssign() {
            var cancelList = [];
            $('input:checkbox[name=checkbox3]:checked').each(function(idx, item){
                cancelList.push(item.value);
            });

            var param = {"cancelList": cancelList};
            jQuery.ajaxSettings.traditional = true;
            $.ajax({
                method: 'POST',
                url: '/cancelAssign',
                data: param,
                beforeSend : function(xhr) {
                    /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
                    xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
                },
            }).done(function (data) {
                if(data === "SUCC"){
                    $('#successDialog').dialog('open');
                } else if(data === "FAIL") {
                    $('#failDialog').dialog('open');
                }
            }).fail(function (data) {
                $('#failDialog').dialog('open');
            });
        };

        function initSort(flag) {
            var element;
            var dir;

            if(flag === 1) {
                dir = direction;
                element = $('#' + sortingTarget);
            } else if (flag === 2) {
                dir = direction2;
                element = $('#' + sortingTarget2);
            }

            var backup = element.html();
            backup = backup.substring(backup.indexOf('<i>')).trim();

            if(dir === 'asc') {
                element.html(backup + ' <i class="fas fa-long-arrow-alt-down"></i>');
            } else if(dir === 'desc') {
                element.html(backup + ' <i class="fas fa-long-arrow-alt-up"></i>');
            } else {
                element.html(backup);
            }
        }

        function onChangeSort(id, flag) {
            var target;
            var dir;
            if(flag === 1) {
                target = sortingTarget;
                dir = direction;
            } else if (flag === 2) {
                target = sortingTarget2;
                dir = direction2
            }

            if(target !== id) {
                target = id;
                dir = 'asc';
            } else {
                if(dir === 'asc') {
                    dir = 'desc';
                } else if (dir === 'desc') {
                    target = '';
                    dir = '';
                } else {
                    dir = 'asc';
                }
            }

            if(flag === 1) {
                $('#sortingTarget').val(target);
                $('#direction').val(dir);
            } else if (flag === 2) {
                $('#sortingTarget2').val(target);
                $('#direction2').val(dir);
            }

            //TODO: develop 브랜치로 merge 후 sorting에 대한 goSerch 매개변수 조건 추가해야함. / 검색시 sorting 관련 변수 초기화도 필요 [20190911 by Maro Kim]
            goSearch();
        }

        function goSearch(condition){
            if(condition ==  true) {
                $('#currentPage').val(1);
                $('#currentPage2').val(1);
                $('#currentPage3').val(1);
            }

            var schCampId      = $('#schTopCampId').val();
            var schMntType     = $('#schTopMntType').val();
            var schOpId        = $('#schTopOpId').val();
            var schTargetDt    = $('#schTopTargetDt').val();
            var schAssignedYn = $('#schTopAssignedYn').val();
            var schCustNm     = $('#schTopCustNm').val();
            var schCustId      = $('#schTopCustId').val();

            $('#schCampId').val(schCampId);
            $('#schMntType').val(schMntType);
            $('#schOpId').val(schOpId);
            $('#schTargetDt').val(schTargetDt);
            $('#schAssignedYn').val(schAssignedYn);
            $('#schCustNm').val(schCustNm);
            $('#schCustId').val(schCustId);

            valueForm.action = "/opAssign";
            valueForm.method = "POST";
            valueForm.submit();
        }

        function getCheckedContractData() {
            var checkedArr = [];
            $('input:checkbox[name=checkbox1]:checked').each(function(idx, item){
                checkedArr.push(item.value);
            });
            return checkedArr;
        }

        function getCheckedOpData() {
            var result = false;
            var checkedData = $('input:checkbox[name=checkbox2]:checked');
            if(!checkedData || checkedData.length !== 1) {
                return false;
            } else {
                $(checkedData.each(function(idx, item){
                    result = item.value;
                }));
            }
            return result;
        }

        //대상 리스트 전체 체크
        $("#checkBox00").click(function () {
            var obj = document.getElementsByName("checkbox1");
            for (var i = 0; i < obj.length; i++) {
                obj[i].checked = !obj[i].checked;
            }
        });

        //상담사별 상세 전체 체크
        $("#checkBox00-03").click(function () {
            var obj = document.getElementsByName("checkbox3");
            for (var i = 0; i < obj.length; i++) {
                obj[i].checked = !obj[i].checked;
            }
        });

        // 배정현황 하나만 체크되도록
        $('input:checkbox[name=checkbox2]').click(function() {
            var element = this;
            $('input:checkbox[name=checkbox2]').each(function(idx, item){
                if(element !== item) {
                    $(this).attr("checked", false);
                }
            });
        });

        //대상 리스트 페이징 처리
        function goPage(cp) {
            $('#currentPage').val(cp)
            goSearch();
        }


        //배정현황 페이징 처리
        function op_goPage(cp) {
            $('#currentPage2').val(cp)
            goSearch();
        }

        //상담사별 상세 페이징 처리
        function detail_goPage(cp) {
            $('#currentPage3').val(cp)
            goSearch();
        }

        function check_assign_click(custOpId) {
            $('#schOpIdDetail').val(custOpId);
            $('#currentPage3').val(1);

            valueForm.action = "/opAssign";
            valueForm.method = "POST";
            valueForm.submit();
        }

        //엔터값 체크
        function enterCheck(event){
            console.log(window.event.keyCode);
            if( window.event.keyCode == 13 ){
                goSearch(true);
            }else{
                return false;
            }
        }
    </script>

</form>

</body>
</html>
