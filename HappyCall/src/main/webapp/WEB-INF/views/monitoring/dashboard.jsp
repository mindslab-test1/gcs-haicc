<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <%@ include file="../common/header_headLink.jsp" %>

    <title>Happy Call</title>
</head>
<body>

<form id="valueForm" name="valueForm">

    <!-- .page loading -->
    <div id="page_ldWrap" class="page_loading">
        <div class="loading_itemBox"><span></span><span></span><span></span><span></span></div>
    </div>
    <!-- //.page loading -->

    <!-- wrap -->
    <div id="wrap">
        <!-- header -->
        <%@ include file="../common/header_headProfile.jsp" %>
        <!-- //header -->

        <!-- container -->
        <div id="container">
            <!-- snb -->
            <%@ include file="../common/leftMenu.jsp" %>
            <!-- //snb -->

            <!-- contents -->
            <div id="contents">

            </div>
            <!-- //contents -->
        </div>
        <!-- //container -->

        <!-- footer -->
        <div id="footer">

        </div>
        <!-- //footer -->
    </div>
    <!-- //wrap -->


    <!-- javascript link & init -->
    <%@ include file="../common/footer_init.jsp" %>
    <!-- //javascript link & init -->

    <script type="text/javascript">

    </script>

</form>

</body>

</html>
