<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<%@ include file="../common/header_headLink.jsp" %>

<title>Happy Call</title>
</head>
<body>

<form id="valueForm" name="valueForm">

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading"><div class="loading_itemBox"><span></span><span></span><span></span><span></span></div></div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap"> 
	<!-- header -->
	<%@ include file="../common/header_headProfile.jsp" %>
	<!-- //header -->
	
	<!-- container -->
	<div id="container">
		<!-- snb -->
		<%@ include file="../common/leftMenu.jsp" %>
		<!-- //snb -->
		
		<!-- contents -->
		<div id="contents">

			<!-- Search BOX -->
			<%@ include file="../common/outboundResultHeaderSearch.jsp" %>
			<!-- //Search BOX -->

			<!-- data list -->
			<div class="data_list tbl_thead">
				<table>
					<colgroup>
						<col style="width:45px;"><col style="width:auto">
						<col style="width:6%;"><col style="width:10%;"><col style="width:8%;">
						<col style="width:6%;"><col style="width:12%;"><col style="width:6%;">
						<col style="width:7%;"><col style="width:15%;"><col style="width:8%;">
					</colgroup>
					<thead>
					<tr>
						<th scope="col">No.</th>
						<th id='CAMPAIGN_NM' scope="col" onclick="onChangeSort(this.id)">모니터링 종류</th>
						<th id='CUST_NM' scope="col" onclick="onChangeSort(this.id)">고객명</th>
						<th id='CUST_ID' scope="col" onclick="onChangeSort(this.id)">고객관리번호</th>
						<th id='TARGET_DT' scope="col" onclick="onChangeSort(this.id)">대상일자</th>
						<th id='CUST_OP_ID' scope="col" onclick="onChangeSort(this.id)">상담사명</th>
						<th id='CALL_DATE' scope="col" onclick="onChangeSort(this.id)">최근통화일시</th>
						<th id='CALL_TRY_COUNT' scope="col" onclick="onChangeSort(this.id)">콜횟수</th>
						<th id='CALL_STATUS_NM' scope="col" onclick="onChangeSort(this.id)">콜상태</th>
						<th id='MNT_STATUS_NAME' scope="col" onclick="onChangeSort(this.id)">모니터링 내용</th>
						<th scope="col">최종 결과</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="data_list tbl_tbody">
				<table>
					<colgroup>
						<col style="width:45px;"><col style="width:auto">
						<col style="width:6%;"><col style="width:10%;"><col style="width:8%;">
						<col style="width:6%;"><col style="width:12%;"><col style="width:6%;">
						<col style="width:7%;"><col style="width:15%;"><col style="width:8%;">
					</colgroup>
					<tbody>
		<c:choose>
			<c:when test="${fn:length(list) gt 0}">
				<c:forEach items="${list}" var="list" varStatus="status">										
					<tr>
						<td>${(frontMntVO.startRow + status.index + 1)}</td> 
						<td>${list.campaignNm}</td>
						<td>${list.custNm}</td>
						<td>${list.custId}</td>
						<td>${list.targetDt}</td>
						<td>${list.custOpNm}</td>
						<td>${list.callDate}</td>
						<td>
							<div class="tooltip_info">
								<a href="#none">${list.callTryCount}</a>
								<c:if test="${list.callTryCount gt 0}">
											<!-- 콜횟수 상세 팝업 - 위로 보여주기 : class명 : tooltip_top -->
									<c:choose>
										<c:when test="${status.index lt 4}">
											<div class="tooltip_bottom">
										</c:when>
										<c:otherwise>
											<div class="tooltip_top">
										</c:otherwise>
									</c:choose>
									<!-- table -->
									<div class="tbl_popList">
										<div class="head">
											<table>
												<caption>콜횟수 상세보기</caption>
												<colgroup>
													<col style="width:40px;"><col style="width:105px;"><col style="width:50px;"><col style="width:135px;"><col style="width:135px;">
												</colgroup>
												<thead>
												<tr>
													<th scope="col">회차</th><th scope="col">통화일시</th><th scope="col">콜 상태</th><th scope="col">모니터링 내용</th><th scope="col">메모</th>
												</tr>
												</thead>
											</table>
										</div>
										<div class="body">
											<table>
												<caption>콜횟수 상세보기</caption>
												<colgroup>
													<col style="width:40px;"><col style="width:105px;"><col style="width:50px;"><col style="width:135px;"><col style="width:135px;">
												</colgroup>
												<tbody>
													<c:choose>
														<c:when test="${fn:length(list.callHistDTO) gt 0}">
															<c:forEach items="${list.callHistDTO}" var="subList" varStatus="subStatus">
																<tr>
																	<td class="txt_center">${fn:length(list.callHistDTO) - subStatus.index}</td>
																	<td class="txt_center">${subList.callDate}</td>
																	<td>${subList.chCallStatusNm}</td>
																	<td>${subList.mntStatusName}</td>
																	<td>${subList.callMemo}</td>
																</tr>
															</c:forEach>
														</c:when>
													</c:choose>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- //콜횟수 상세 팝업 - 위로 보여주기 : class명 : tooltip_top -->
								</c:if>
							</div>
						</td>
						<td><i class="fas fa-circle done"></i><em>${list.callStatusNm}</em></td>
						<td><em>${list.mntStatusName}</em></td>
						<td><button type="button" class="btn_detail" onclick="resultDetail(${list.contractNo},${list.callId});"><em>${list.finalResult}</em></button></td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
					<tr>
						<td colspan="13">데이터 없음</td>
					</tr>
			</c:otherwise>
		</c:choose>
					</tbody>
				</table>
			</div>
			<!-- //data list -->
			
			<!-- page_area -->
			<div class="page_area">	
				<%@ include file="../common/paging.jsp" %>
			</div>
			<!-- //page_area -->
			
					
			<input type="hidden" name="checkedChkBox" id="checkedChkBox" />
			
			<input type="hidden" id="schCampId" name="schCampId" />
			<input type="hidden" id="schMntType" name="schMntType" />
			<input type="hidden" id="schOpNm" name="schOpNm" />
			<input type="hidden" id="schTargetDt" name="schTargetDt" />
			<input type="hidden" id="schFinalResult" name="schFinalResult" />
			<input type="hidden" id="schCallCnt" name="schCallCnt" />
			<input type="hidden" id="schCustNm" name="schCustNm" />
			<input type="hidden" id="schCustId" name="schCustId" />
			<input type="hidden" id="schCustTelNo" name="schCustTelNo" />
			<input type="hidden" id="schMemo" name="schMemo" />
			<input type="hidden" id="schCallState" name="schCallState" />
			<input type="hidden" id="schCallResult" name="schCallResult" />

			<input type="hidden" id="sortingTarget" name="sortingTarget" />
			<input type="hidden" id="direction" name="direction" />
			<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}"/>
			
			<input type="hidden" id="currentPage" name="currentPage" value="${paging.currentPage}" /> 
			
		</div>
		<!-- //contents -->
	</div>

	<%--// 각각의 알림창 관련 id--%>
	<div id="checkKey1Dialog" class="dialog"></div>
	<div id="checkKey2Dialog" class="dialog"></div>
	<!-- //container -->
	<!-- footer -->
	<div id="footer">
		
	</div>
	<!-- //footer -->
</div>
<!-- //wrap -->


<!-- javascript link & init -->
<%@ include file="../common/footer_init.jsp" %>
<!-- //javascript link & init -->

<script type="text/javascript">
	var sortingTarget = "${frontMntVO.sortingTarget}";
	var direction = "${frontMntVO.direction}";

$(document).ready(function(){
	var rows = '<c:out value="${frontMntVO.pageInitPerPage}" />';
	if( rows != '' ){
		$('#pageInitPerPage').val(rows);
	}

	$('#schTopCampId').val( '<c:out value="${frontMntVO.schCampId}" />' );
	$('#schTopMntType').val( '<c:out value="${frontMntVO.schMntType}" />' );
 	$('#schTopOpNm').val( '<c:out value="${frontMntVO.schOpNm}" />' );
 	$('#schTopTargetDt').val( '<c:out value="${frontMntVO.schTargetDt}" />' );
 	$('#schTopFinalResult').val( '<c:out value="${frontMntVO.schFinalResult}" />' );
 	$('#schTopCallCnt').val( '<c:out value="${frontMntVO.schCallCnt}" />' );
 	$('#schTopCustNm').val( '<c:out value="${frontMntVO.schCustNm}" />' );
 	$('#schTopCustId').val( '<c:out value="${frontMntVO.schCustId}" />' );
 	$('#schTopCustTelNo').val( '<c:out value="${frontMntVO.schCustTelNo}" />' );
 	$('#schTopMemo').val( '<c:out value="${frontMntVO.schMemo}" />' );
 	$('#schTopCallState').val( '<c:out value="${frontMntVO.schCallState}" />' );
 	$('#schTopCallResult').val( '<c:out value="${frontMntVO.schCallResult}" />' );

	$('#sortingTarget').val( '<c:out value="${frontMntVO.sortingTarget}" />' );
	$('#direction').val( '<c:out value="${frontMntVO.direction}" />' );

	if(sortingTarget) {
		initSort();
	}

	var checkKey1DialogMsg = "숫자/영문/일부 특수기호('.', '-', '\\')만 입력 가능합니다.";
	var checkKey1DialogId = 'checkKey1Dialog';
	var checkKey1DialogType = 'checkKey1';
	setDialog(checkKey1DialogId, checkKey1DialogMsg, checkKey1DialogType);

	var checkKey2DialogMsg = "숫자만 입력 가능합니다.";
	var checkKey2DialogId = 'checkKey2Dialog';
	var checkKey2DialogType = 'checkKey2';
	setDialog(checkKey2DialogId, checkKey2DialogMsg, checkKey2DialogType);
});

function initSort() {
	var backup = $('#' + sortingTarget).html();
	backup = backup.substring(backup.indexOf('<i>')).trim();
	if(direction === 'asc') {
		$('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-down"></i>');
	} else if(direction === 'desc') {
		$('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-up"></i>');
	} else {
		$('#' + sortingTarget).html(backup);
	}
}

function onChangeSort(id) {
	if(sortingTarget !== id) {
		sortingTarget = id;
		direction = 'asc';
	} else {
		if(direction === 'asc') {
			direction = 'desc';
		} else if (direction === 'desc') {
			sortingTarget = '';
			direction = '';
		} else {
			direction = 'asc';
		}
	}
	$('#sortingTarget').val(sortingTarget);
	$('#direction').val(direction);

	//TODO: develop 브랜치로 merge 후 sorting에 대한 goSerch 매개변수 조건 추가해야함. / 검색시 sorting 관련 변수 초기화도 필요 [20190911 by Maro Kim]
	goSearch();
}
//검색실행
function goSearch(condition){
	if(condition ==  true) {
		$('#currentPage').val(1);
	}

	var schCampId      = $('#schTopCampId').val();
	var schMntType     = $('#schTopMntType').val();
	var schOpNm        = $('#schTopOpNm').val();
	var schTargetDt    = $('#schTopTargetDt').val();
	var schFinalResult = $('#schTopFinalResult').val();
	var schCallCnt     = $('#schTopCallCnt').val();
	var schCustNm      = $('#schTopCustNm').val();
	var schCustId     = $('#schTopCustId').val();
	var schCustTelNo   = $('#schTopCustTelNo').val();
	var schMemo        = $('#schTopMemo').val();
	var schCallState   = $('#schTopCallState').val();
	var schCallResult  = $('#schTopCallResult').val();

	$('#schCampId').val(schCampId);
	$('#schMntType').val(schMntType);
	$('#schOpNm').val(schOpNm);
	$('#schTargetDt').val(schTargetDt);
	$('#schFinalResult').val(schFinalResult);
	$('#schCallCnt').val(schCallCnt);
	$('#schCustNm').val(schCustNm);
	$('#schCustId').val(schCustId);
	$('#schCustTelNo').val(schCustTelNo);
	$('#schMemo').val(schMemo);
	$('#schCallState').val(schCallState);
	$('#schCallResult').val(schCallResult);
	
	valueForm.method = "POST";
	valueForm.action = "/mntResult";
	valueForm.submit();
}

//페이지 이동 관련
function goPage(cp){
	$('#currentPage').val(cp);
	
	goSearch(false);
}

function setDialog(id, msg, type) {
	var buttons;

    buttons = [
        {
            text: "Cancel",
            click: function () {
                $(this).dialog("close");
                location.reload();
            }
        }
    ];

	$('#'+id).html(msg);
	$('#'+id).dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: buttons
	});
}

function resultDetail(contract_no, call_id){
	var popSize = "width=800,height=600";
	var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no";
	winpop1 = window.open("/mntResultPop?ctn="+call_id+"&cno="+contract_no,"winpop1", popSize + "," + popOption);
	
}

//키 입력시 허용 값 체크
function checkKey( objName ){
	if( objName == 'campId' ){
		if( check_key_reg(1, $('#schTopCampId').val() ) == false ){
			$('#schTopCampId').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'targetDt' ){
		if( check_key_reg(1, $('#schTopTargetDt').val() ) == false ){
			$('#schTopTargetDt').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'callCnt' ){
		if( check_key_reg(2, $('#schTopCallCnt').val() ) == false ){
			$('#schTopCallCnt').val('');
			$('#checkKey2Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'custId' ){
		if( check_key_reg(1, $('#schTopCustId').val() ) == false ){
			$('#schTopCustId').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}		
	}else if( objName == 'custTelNo' ){
		if( check_key_reg(1, $('#schTopCustTelNo').val() ) == false ){
			$('#schTopCustTelNo').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}
	}
}

//값 체크 정규식 함수
function check_key_reg( mode, text ){
	if( mode == 1 ){
	    var regexp = /[0-9a-zA-Z.\-\\/]/; 	//숫자,영문,특수문자 허용
	    
	}else if( mode == 2 ){
		var regexp = /[0-9]/; 				// 숫자만 허용
		
	}else if( mode == 2 ){
		var regexp = /[0-9.\-\\/]/; 		// 숫자, '. - \ /'만 허용
		
	}else if( mode == 4 ){
		var regexp = /[a-zA-Z]/; 			// 영문만 허용
		
	}
  
  for( var i=0; i<text.length; i++){
      if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false ){
			return false;
      }
  }  
}

//엔터값 체크
function enterCheck(){
	if( window.event.keyCode == 13 ){
		goSearch(true);
	}else{
		return false;
	}
}
</script>

</form>

</body>

</html>










