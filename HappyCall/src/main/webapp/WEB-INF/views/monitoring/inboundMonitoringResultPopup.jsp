﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<%@ include file="../common/header_headLink.jsp" %>
<title>Happy Call</title>
</head>
<body style="width:800px; height:600px;">
<div id="popup_wrap">
	<!-- popup_header -->
	<div class="popup_header">
		<h1>모니터링 <span>실행 결과</span></h1>
	</div>
	<!-- //popup_header -->

	<!-- popup_contents -->
	<div class="popup_contents">
		<!-- customInfo_wrap -->
		<div class="customInfo_wrap">
			<div class="custom_list">
				<table>
					<caption>고객 상세</caption>
					<colgroup>
						<col style="width:100px">
						<col style="width:auto">
					</colgroup>
					<tbody>
					<tr>
						<th scope="row">고객명</th>
						<td>${topInfo.custNm}</td>
					</tr>
					<tr>
						<th scope="row">전화번호</th>
						<td>${topInfo.telNo}</td>
					</tr>
					<tr>
						<th scope="row">통화일시</th>
						<td>${topInfo.callDate}</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="custom_list">
				<table>
					<caption>고객 상세</caption>
					<colgroup>
						<col style="width:100px">
						<col style="width:auto">
					</colgroup>
					<tbody>
					<tr>
						<th scope="row">대출금액</th>
						<td>
							<span></span>
							<span></span>
						</td>
					</tr>
					<tr>
						<th scope="row">입금계좌 정보</th>
						<td></td>
					</tr>
					<tr>
						<th scope="row">자동이체 일자</th>
						<td></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //customInfo_wrap -->
		<!-- cont_left -->
		<div class="cont_left">
			<div class="monitor_state"><!-- 비활성화일 때 class에 off를 추가해 주세요 -->
				<i class="fas fa-grin-beam"><span class="hide">Good</span></i>
				<i class="fas fa-grin-beam-sweat off"><span class="hide">Not Good</span></i>
				<i class="fas fa-tired off"><span class="hide">Bad</span></i>
			</div>
			<!-- call_box -->
			<div class="call_box">
				<!-- call_player -->
				 <div class="call_player">
					<audio src="http://${audioUrl}/call/record/${frontMntVO.callId}/${frontMntVO.cno}/${frontMntVO.campaignId}" preload="auto" controls></audio>
				</div>
				<!-- //call_player -->
				<!-- call_cont -->
				<div class="call_cont">
					<!-- call_list -->
					<ul id="call_list" class="call_list">
	<c:choose>
		<c:when test="${fn:length(sttResult) gt 0}">
			<c:forEach items="${sttResult}" var="sttResultList" varStatus="status">
				<c:choose>
					<c:when test="${sttResultList.speakerCode eq 'ST0002'}">
						<!-- agent -->
						<li class="agent"> 
							<span class="thumb"><i class="fas fa-robot"></i><span class="speaker">AI</span></span>
							<span class="cont"><em class="txt">${sttResultList.sentence}</em></span> 
						</li>
						<!-- //agent -->
					</c:when>
					<c:otherwise>
						<!-- client -->
						<li class="client">
							<span class="thumb"><i class="fas fa-user"></i><span class="speaker">고객</span></span>
							<span class="cont"><em class="txt">${sttResultList.sentence}</em></span>
						</li>
						<!-- //client -->
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</c:when>
		<c:otherwise></c:otherwise>
	</c:choose>
	
					</ul>
					<!-- //call_list -->
				</div>
				<!-- //call_cont -->
			</div>
			<!-- //call_box -->
		</div>
		<!-- //cont_left -->
		<!-- cont_right -->
		<div class="cont_right">
			<!-- 모니터링 결과 -->
			<h2>모니터링 결과</h2>
			<div class="tbl_customTh">
				<table>
					<caption>모니터링 결과 제목</caption>
					<colgroup>
						<col style="width:40px">
						<col style="width:auto">
						<col style="width:134px">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">No.</th>
							<th scope="col">구간</th>
							<th scope="col">탐지</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="tbl_customTd">
				<table>
					<caption>모니터링 결과 내용</caption>
					<colgroup>
						<col style="width:40px">
						<col style="width:auto">
						<col style="width:128px">
					</colgroup>
					<tbody>
		<c:choose>
			<c:when test="${fn:length(mntResult) gt 0}">
				<c:forEach items="${mntResult}" var="mntResult" varStatus="status">					
					<tr>
						<td class="txt_center">${status.index + 1}</td>
						<td>${mntResult.taskInfo}</td>
						<td>
						
							<c:choose>
								<c:when test="${score[mntResult.seq] eq 'Y'}">
									<div class="radiobox form_type01">
										<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}" checked disabled="disabled"><label for="resultYes${mntResult.seq}">Yes</label>
									</div>
									<div class="radiobox form_type01">
										<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}" disabled="disabled"><label for="resultNo${mntResult.seq}">No</label>
									</div>
								</c:when>
								<c:when test="${score[mntResult.seq] eq 'N'}">
									<div class="radiobox form_type01">
										<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}" checked disabled="disabled"><label for="resultYes${mntResult.seq}">Yes</label>
									</div>
									<div class="radiobox form_type01">
										<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}" checked disabled="disabled"><label for="resultNo${mntResult.seq}">No</label>
									</div>
								</c:when>
								<c:otherwise>
									<div class="radiobox form_type01">
										<input type="radio" id="resultYes${mntResult.seq}" name="resultYes${mntResult.seq}" disabled="disabled"><label for="resultYes${mntResult.seq}">Yes</label>
									</div>
									<div class="radiobox form_type01">
										<input type="radio" id="resultNo${mntResult.seq}" name="resultYes${mntResult.seq}" disabled="disabled"><label for="resultNo${mntResult.seq}">No</label>
									</div>								
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</c:when>
		</c:choose>	
					</tbody>
				</table>
			</div>
			<!-- //모니터링 결과 -->
			<!-- 상담 메모 -->
			<div class="textarea_wrap single02">
				<textarea id="popMemo" rows="3" cols="50" onKeyup="checkPopMemoLen()" placeholder="상담 메모 (글자수 최대 255자)" class="textarea_box">${memo}</textarea>
				<span id="popMemoLen">0/255</span>
			</div>
			<!-- //상담 메모 -->
		</div>
		<!-- //cont_right -->
	</div>
	<!-- //popup_contents -->
	<!-- popup_bottom -->
	<div class="popup_bottom">
		<div class="btnPage_wrap single">
			<div class="popup_paging"></div>
		</div>
		<div id="sign_calling" class="txt_state">
			<!-- <p class="headset"><i class="fas fa-headset"></i> 고객과 직접 통화중</p> -->
			<%--<p class="robot"><i class="fas fa-robot"></i>고객과 AI 통화중</p>--%>
			<p class="robot"><i></i></p>
		</div>
		<%--<div class="btnPage_wrap_left">
			&lt;%&ndash;<button type="button" id="call_change_op" class="btn_default02">콜 상담사로 전환</button>&ndash;%&gt;
		</div>--%>
		<div class="btnPage_wrap">
			<div style="display: flex; justify-content: space-between;">
				<button type="button" class="btn_default01" onClick="javascript:confirmSave();">메모 저장</button>
				<button type="button" class="btn_default01" onClick="javascript:self.close();">확인</button>
			</div>

			<%--// 각각의 알림창 관련 id--%>
			<div id="successDialog" class="dialog"></div>
			<div id="failDialog" class="dialog"></div>
			<div id="saveDialog" class="dialog"></div>
			<div id="confirmDialog" class="dialog"></div>

		</div>
	</div>
	<!-- //popup_bottom -->
</div>
<input type="hidden" id="cno" name="cno" value="${frontMntVO.cno}" />
<input type="hidden" id="ctn" name="ctn" value="${frontMntVO.ctn}" />
<!-- javascript link & init -->
<%@ include file="../common/footer_init_popup.jsp" %>
<!-- //javascript link & init -->

<script type="text/javascript">
$(document).ready(function(){

	init();

	$("#selCtn").val(${frontMntVO.ctn}).attr("selected","selected");

	checkPopMemoLen();	// 팝업 실행 시 메모 길이 체크

	var call_now = $("#selCtn option").index($("#selCtn option:selected"));
	var call_total = $("#selCtn option").size()-1;
	audioPlayerInit();
});

//실시간 메세지 관련 팝업 실행시 값 설정 및 초기화
function init(){
	var successDialogMsg = '저장 완료되었습니다.';
	var successDialogId = 'successDialog';
	var successDialogType = 'success';
	setDialog(successDialogId, successDialogMsg, successDialogType);

	var failDialogMsg = '저장 실패되었습니다.';
	var failDialogId = 'failDialog';
	var failDialogType = 'fail';
	setDialog(failDialogId, failDialogMsg, failDialogType);

	var saveDialogMsg = '상담메모는 최대 255자까지 가능합니다.';
	var saveDialogId = 'saveDialog';
	var saveDialogType = 'save';
	setDialog(saveDialogId, saveDialogMsg, saveDialogType);

	var confirmDialogMsg = '저장하시겠습니까?';
	var confirmDialogId = 'confirmDialog';
	var confirmDialogType = 'confirm';
	setDialog(confirmDialogId, confirmDialogMsg, confirmDialogType);

	checkPopMemoLen();
}

function confirmSave() {
	$('#confirmDialog').dialog('open');
}

// 메모 저장
function doPopSave(){

	var cno = $('#cno').val();
	var callId = '<c:out value="${topInfo.callId}" />';
	var campId = '<c:out value="${topInfo.campaignId}" />';
	var popMemo = $('#popMemo').val();
	var popMntCont = $('#popMntCont').val();

	if( popMemo.length > 255 ) {
		$('#saveDialog').dialog('open');
		return false;
	}

	$.ajax({
		url : "/ibMntPopSave",
		data : {cno:cno, callId:callId, campaignId:campId, popMemo:popMemo},
		type : "POST",
		beforeSend : function(xhr) {
			/*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
			xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
		},
	}).done(function(data){
		if(data == "SUCC"){
			$("#call_end_next").attr("disabled", false);				//저장되었으므로 "다음 콜 걸기"버튼 활성화
			$('#successDialog').dialog('open');
		} else {
			$('#failsDialog').dialog('open');
		}
	}).fail(function(data){
		if(data == "FAIL"){												//저장 실패이므로 추가 액션 없음.
			$('#failsDialog').dialog('open');
		}
	});
}

function audioPlayerInit() {
	$('#audioPlayer').each(function(){
	    this.pause(); // Stop playing
	    this.currentTime = 0; // Reset time
	});
	
	try{
		$('.audioplayer-playpause').attr( 'title','play');
	    $('.audioplayer-playpause a').html( 'Play');
	    $('.audioplayer').removeClass('audioplayer-playing');
	    $('.audioplayer').addClass('audioplayer-stopped');	
	}catch(e){
		console.log('audio stop exception',e);
	}
}
function updateTrackTime(track){
	var currTime = Math.floor(track.currentTime);
	var duration = Math.floor(track.duration);
	
	console.log("### ct:" + currTime + ", dt:" + duration);
}
//상담메모 입력 문자 길이 체크
function checkPopMemoLen(){
	var popMemo = $('#popMemo').val();
	$('#popMemoLen').html(popMemo.length + "/255");
}

function setDialog(id, msg, type) {
	var buttons;
	if(type === 'confirm') {
		buttons= [
			{
				text: "OK",
				click: function() {
					doPopSave();
					$(this).dialog("close");
				}
			},
			{
				text: "Cancel",
				click: function() {
					$(this).dialog("close");
				}
			}
		];
	} else {
		buttons = [
			{
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
				}
			}
		];
	}

	$('#'+id).html(msg);
	$('#'+id).dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: buttons
	});
}
</script>
</body>
</html>





