<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <%@ include file="../common/header_headLink.jsp" %>

    <title>Happy Call</title>
</head>

<body>

<form id="valueForm" name="valueForm">


    <!-- .page loading -->
    <div id="page_ldWrap" class="page_loading">
        <div class="loading_itemBox"><span></span><span></span><span></span><span></span></div>
    </div>
    <!-- //.page loading -->

    <!-- wrap -->
    <div id="wrap">
        <!-- header -->
        <%@ include file="../common/header_headProfile.jsp" %>
        <!-- //header -->

        <!-- container -->
        <div id="container">
            <!-- snb -->
            <%@ include file="../common/leftMenu2.jsp" %>
            <!-- //snb -->

            <!-- contents -->
            <div id="contents">
                <!-- 자동 모니터링 결과 -->
                <div style="display: flex">
                    <div class="total_wrap_b" style="width: 100% !important; flex: 1;">
                        <ul style="display: flex; justify-content: space-around;">
                            <li>
                                <dl>
                                    <dt>전체 콜 수</dt>
                                    <dd><em id="sumAll">0</em>건</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt><i class="fas fa-circle ing"></i>진행중</dt>
                                    <dd><em id="sumIng">0</em>건</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt><i class="fas fa-circle error"></i>오류</dt>
                                    <dd><em id="sumErr">0</em>건</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt><i class="fas fa-circle transfer"></i>호전환</dt>
                                    <dd><em id="sumTransfer">0</em>건</dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt><i class="fas fa-circle done"></i>완료</dt>
                                    <dd><em id="sumDone">0</em>건</dd>
                                </dl>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="total_wrap_c" style="display: flex; justify-content: flex-end; align-items: center;">
                    <div class="btn_result">
                        <button type="button" id="latestCallList" class="btn_default01" onclick="runEffect()">최근통화리스트</button>
                    </div>
                </div>
                <!-- //자동 모니터링 결과 -->
                <div class="call-all">
                    <div class="telephone-all">
                        <table>
                            <c:forEach items="${phoneListResult}" var="phoneItem" varStatus="statusMnt">
                                <c:if test="${(statusMnt.count%5 eq 1)}">
                                    <tr>
                                </c:if>
                                <td>
                                    <div id="${phoneItem.sipUser}"
                                         style="display: block; margin: 10px; text-align: center; cursor: pointer;">
                                        <c:set var="telLength" value="${fn:length(phoneItem.telUri)}"/>
                                        <c:choose>
                                            <c:when test="${phoneItem.status eq 'CS0002'}">
                                                <div name="statusIcon"><i style="color:green;"
                                                                          onclick="runAutoPop('${phoneItem.sipUser}')"
                                                                          class="fas fa-phone fa-10x"></i></div>
                                            </c:when>
                                            <c:otherwise>
                                                <div name="statusIcon"><i class="fas fa-pause-circle fa-10x"></i></div>
                                            </c:otherwise>
                                        </c:choose>
                                        <div name="number">${phoneItem.sipUser}</div>
                                        <c:choose>
                                            <c:when test="${telLength eq 8}">
                                                <div name="exNumber">
                                                        ${fn:substring(phoneItem.telUri,0,4)}-
                                                        ${fn:substring(phoneItem.telUri,4,8)}
                                                </div>
                                            </c:when>
                                            <c:when test="${telLength eq 11}">
                                                <div name="exNumber">
                                                        ${fn:substring(phoneItem.telUri,0,3)}-
                                                        ${fn:substring(phoneItem.telUri,3,7)}-
                                                        ${fn:substring(phoneItem.telUri,7,12)}
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                ${phoneItem.telUri}
                                            </c:otherwise>
                                        </c:choose>
                                        <div name="contractNo" style="display: none;">${phoneItem.contractNo}</div>
                                    </div>
                                </td>
                                <c:if test="${(statusMnt.count%5 eq 0 || statusMnt.end)}">
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </table>
                    </div>

                    <%--toggle slide 추가--%>
                    <div class="toggler">
                        <div id="effect" class="ui-widget-content ui-corner-all">
                            <!-- 최근통화리스트 -->
                            <div class="data_list tbl_thead">
                                <table>
                                    <colgroup>
                                        <col style="width:13%;">
                                        <col style="width:12%;">
                                        <col style="width:12%;">
                                        <%--  <col style="width:9%;">
                                          <col style="width:14%;">--%>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th scope="col">모니터링 종류</th>
                                        <th scope="col">고객전화번호</th>
                                        <th scope="col">통화일시</th>
                                        <%--           <th scope="col">콜상태</th>
                                                   <th scope="col">모니터링 내용</th>--%>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="data_list tbl_tbody">
                                <table>
                                    <tbody>
                                    <colgroup>
                                        <col style="width:13%;">
                                        <col style="width:12%;">
                                        <col style="width:12%;">
                                        <%-- <col style="width:9%;">
                                         <col style="width:14%;">--%>
                                    </colgroup>
                                    <c:choose>
                                        <c:when test="${fn:length(latestCallList) gt 0}">
                                            <c:forEach items="${latestCallList}" var="callList" varStatus="status">
                                                <tr>
                                                    <td>${callList.campaignNm}</td>
                                                    <td>${callList.telNo}</td>
                                                    <td>${callList.callDate}</td>
                                                </tr>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>
                                            <tr>
                                                <td colspan="13">데이터 없음</td>
                                            </tr>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </div>
                            <!-- //data list -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //container -->
    </div>
    <!-- //wrap -->
    <!-- javascript link & init -->
    <%@ include file="../common/footer_init.jsp" %>
    <!-- //javascript link & init -->

    <script type="text/javascript">

        $(document).ready(function () {
            getCallStatus();
            conn_ws();
        });

        function runEffect() {
            $('.telephone-all').toggleClass('telephone-all_active', 2000);
            $('.toggler').toggleClass('toggler_active', 2000);
        };

        //websocket main function
        function conn_ws() {

            ws = new WebSocket('${websocketUrl}/callsocket');
            ws.onopen = function () {
                var sendMsg = '{"EventType":"CALL", "Event":"subscribe"}';
                ws.send(sendMsg);
            };

            ws.onmessage = function (e) {
                var rcv_data = JSON.parse(e.data);

                if (rcv_data['EventType'] === 'CALL') {
                    if (rcv_data['Event'] === 'status') {
                        makeHtmlCallStatus(rcv_data);
                    }
                }
            };
        }

        function runAutoPop(phoneNumber) {

            var contractNo = $("#" + phoneNumber).find("div[name=contractNo]").html();

            var popSize = "width=800,height=600";
            var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no";
            var pop1 = window.open("/inboundPop?number=" + phoneNumber + "&pcnt=1&chk_cnt=1" + "&cno=" + contractNo, "pop1", popSize + "," + popOption);
        }

        function makeHtmlCallStatus(data) {

            var readyHtml = "<i class='fas fa-pause-circle fa-10x'>";
            var dialingHtml = "<i style='color:green;' class='fas fa-phone fa-10x'>";
            var callingHtml = "<i style='color:green;' class='fas fa-phone fa-10x'>";

            var item = $("#" + data["Agent"]);
            var contractNo = data['contract_no'];

            if (data["call_status"] == "CS0002") {
                $(item).find("div[name=statusIcon]").html(callingHtml);
                $(item).find("div[name=contractNo]").html(contractNo);

                $(item).find("div[name=statusIcon] i").click(function () {
                    var popSize = "width=800,height=600";
                    var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no";
                    var pop1 = window.open("/inboundPop?number=" + data["Agent"] + "&pcnt=1&chk_cnt=1" + "&cno=" + contractNo, "pop1", popSize + "," + popOption);
                });
            } else if (data["call_status"] == "CS0006") {
                $(item).find("div[name=statusIcon]").html(dialingHtml);
            } else if (data["call_status"] == "CS0003") {
                $(item).find("div[name=statusIcon]").html(readyHtml);
            } else {
                $(item).find("div[name=statusIcon]").html(readyHtml);
            }
            getCallStatus();
        }

        function getCallStatus() {
            //TODO: 최근 통화리스트도 api로 구현되어 이곳에서 업데이트 되어야함. By Maro 2019.06.19
            var protocol = location.protocol + '//';
            var host = location.host;
            var suffix = '/api/inboundCallStatus';
            var url = protocol + host + suffix;

            $('#sumIng').html(0);
            $('#sumTransfer').html(0);
            $('#sumErr').html(0);
            $('#sumDone').html(0);
            $('#sumAll').html(0);

            $.get(url, function (result) {
                if (result) {
                    console.log(result);
                    var sum = 0;
                    result.forEach(function (item, idx) {
                        sum += item['count'];
                        if (item['callStatus'] === 'CS0002') {
                            // AI 진행중
                            $('#sumIng').html(item['count']);
                        } else if (item['callStatus'] === 'CS0009') {
                            // 상담사진행중
                            $('#sumTransfer').html(item['count']);
                        } else if (item['callStatus'] === 'CS0003') {
                            // 중지
                            $('#sumErr').html(item['count']);
                        } else if (item['callStatus'] === 'CS0005') {
                            // AI 종료
                            $('#sumDone').html(item['count']);
                        }

                        if (idx === result.length - 1) {
                            $('#sumAll').html(sum);
                        }
                    });

                } else {
                    // 데이터 획득 실패
                    return;
                }
            })
        }

    </script>
</form>
</body>
</html>
