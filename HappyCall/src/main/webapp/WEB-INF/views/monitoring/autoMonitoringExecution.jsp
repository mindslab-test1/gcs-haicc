<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<%@ include file="../common/header_headLink.jsp" %>

<title>Happy Call</title>
</head>
<body>

<form id="valueForm" name="valueForm">

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading"><div class="loading_itemBox"><span></span><span></span><span></span><span></span></div></div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap"> 
	<!-- header -->
	<%@ include file="../common/header_headProfile.jsp" %>
	<!-- //header -->
	
	<!-- container -->
	<div id="container">
		<!-- snb -->
		<%@ include file="../common/leftMenu.jsp" %>
		<!-- //snb -->
		
		<!-- contents -->
		<div id="contents">
		
			<!-- Search BOX -->
			<%@ include file="../common/outboundHeaderSearch.jsp" %>
			<!-- //Search BOX -->
			
			
			<!-- 자동 모니터링 결과 -->
			<div class="total_wrap">
				<ul style="display: flex; justify-content: space-around;">
					<li>
						<dl>
							<dt>전체 콜 수</dt>
							<dd><em id="sumAll">0</em>건</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt><i class="fas fa-circle waiting"></i>대기</dt>
							<dd><em id="sumWait">0</em>건</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt><i class="fas fa-circle ing"></i>진행중</dt>
							<dd><em id="sumIng">0</em>건</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt><i class="fas fa-circle error"></i>오류</dt>
							<dd><em id="sumErr">0</em>건</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt><i class="fas fa-circle transfer"></i>호전환</dt>
							<dd><em id="sumTransfer">0</em>건</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt><i class="fas fa-circle done"></i>완료</dt>
							<dd><em id="sumDone">0</em>건</dd>
						</dl>
					</li>
                    <li>
                        <dl>
                            <dt><i class="fas fa-circle etc"></i>기타</dt>
                            <dd><em id="sumEtc">0</em>건</dd>
                        </dl>
                    </li>
				</ul>
			</div>
			<!-- //자동 모니터링 결과 -->
			
			
			<!-- data list -->
			<div class="data_list tbl_thead">
				<table>
					<colgroup>
						<col style="width:30px;"><col style="width:45px;"><col style="width:auto;">
						<col style="width:10%;"><col style="width:10%;"><col style="width:10%;">
						<col style="width:10%;"><col style="width:10%;"><col style="width:10%;">
						<col style="width:10%;"><col style="width:15%;">
					</colgroup>
					<thead>
					<tr>
						<th scope="col">
							<div class="checkbox form_type01">
								<input type="checkbox" id="chkBox" onclick="chkBoxAllCheck(this);"><label for="chkBox"></label>
							</div>
						</th>
						<th scope="col">No.</th>
						<th id='CAMPAIGN_NM' scope="col" onclick="onChangeSort(this.id)">모니터링 종류</th>
						<th id='CUST_NM' scope="col" onclick="onChangeSort(this.id)">고객명</th>
						<th id='CUST_ID' scope="col" onclick="onChangeSort(this.id)">고객관리번호</th>
						<th id='TARGET_DT' scope="col" onclick="onChangeSort(this.id)">대상일자</th>
						<th id='CUST_OP_NM' scope="col" onclick="onChangeSort(this.id)">상담사명</th>
						<th id='CALL_DATE' scope="col" onclick="onChangeSort(this.id)">최근통화일시</th>
						<th id='CALL_TRY_COUNT' scope="col" onclick="onChangeSort(this.id)">콜횟수</th>
						<th id='CALL_STATUS_NM' scope="col" onclick="onChangeSort(this.id)">콜상태</th>
						<th id='MNT_STATUS_NAME' scope="col"onclick="onChangeSort(this.id)" >모니터링 내용</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="data_list tbl_tbody">
				<table>
					<colgroup>
						<col style="width:30px;"><col style="width:45px;"><col style="width:auto;">
						<col style="width:10%;"><col style="width:10%;"><col style="width:10%;">
						<col style="width:10%;"><col style="width:10%;"><col style="width:10%;">
						<col style="width:10%;"><col style="width:15%;">
					</colgroup>
					<tbody>
					
		<c:choose>
			<c:when test="${fn:length(list) gt 0}">
				<c:forEach items="${list}" var="mainList" varStatus="status">
					<tr style="cursor: pointer" onclick="runAutoPop('${mainList.contractNo}','${mainList.lastCallId}');">
						<td onclick="event.cancelBubble=true">
							<div class="checkbox form_type01">
								<input type="checkbox" id="chkBox${mainList.contractNo}" name="chkBoxes" value="${mainList.contractNo}" onclick="chkBoxOneCheck(this);"><label for="chkBox${mainList.contractNo}"></label>
								<input type="hidden" id="hidden1${mainList.contractNo}" name="hidden1" value="${mainList.telNo}">
								<input type="hidden" id="hidden2${mainList.contractNo}" name="hidden2" value="${mainList.custOpId}">
								<input type="hidden" id="hidden3${mainList.contractNo}" name="hidden3" value="${mainList.campaignId}">
							</div>
						</td>
						<td>${(frontMntVO.startRow + status.index + 1)}</td>
						<td>${mainList.campaignNm}</td>
						<td>${mainList.custNm}</td>
						<td>${mainList.custId}</td>
						<td>${mainList.targetDt}</td>
						<td>${mainList.custOpNm}</td>
						<td>${mainList.callDate}</td>
						<td>
							<div class="tooltip_info">
								<a href="#none">${mainList.callTryCount}</a>
								
					<c:if test="${mainList.callTryCount gt 0}">
								<!-- 콜횟수 상세 팝업 - 아래로 보여주기 : class명 : tooltip_bottom -->
						<c:choose>
							<c:when test="${status.index lt 4}">								
								<div class="tooltip_bottom">
							</c:when>
							<c:otherwise>
								<div class="tooltip_top">
							</c:otherwise>
						</c:choose>
						<!-- table -->
						<div class="tbl_popList">
							<div class="head">
								<table>
									<caption>콜횟수 상세보기</caption>
									<colgroup>
										<col style="width:40px;"><col style="width:105px;"><col style="width:50px;"><col style="width:135px;"><col style="width:135px;">
									</colgroup>
									<thead>
									<tr>
										<th scope="col">회차</th><th scope="col">통화일시</th><th scope="col">콜 상태</th><th scope="col">모니터링 내용</th><th scope="col">메모</th>
									</tr>
									</thead>
								</table>
							</div>
							<div class="body">
								<table>
									<caption>콜횟수 상세보기</caption>
									<colgroup>
										<col style="width:40px;"><col style="width:105px;"><col style="width:50px;"><col style="width:135px;"><col style="width:135px;">
									</colgroup>
									<tbody>
										<c:choose>
											<c:when test="${fn:length(mainList.callHistDTO) gt 0}">
												<c:forEach items="${mainList.callHistDTO}" var="subList" varStatus="subStatus">
																<tr>
																	<td class="txt_center">${fn:length(mainList.callHistDTO) - subStatus.index}</td>
																	<td class="txt_center">${subList.callDate}</td>
																	<td class="txt_center">${subList.chCallStatusNm}</td>
																	<td>${subList.monitorCont}</td>
																	<td>${subList.callMemo}</td>
																</tr>
												</c:forEach>
											</c:when>
										</c:choose>
									</tbody>
								</table>
							</div>
							<!-- <button type="button" class="btn_toggle02">더보기</button> -->
						</div>
					</div>
					<!-- //콜횟수 상세 팝업 - 아래로 보여주기 : class명 : tooltip_bottom -->
					</c:if>
							</div>
						</td>
						<td>
							<span id="callStatus${mainList.contractNo}"><i class="fas fa-circle done"></i><em>${mainList.callStatusNm}</em></span>
						</td>
						<td><span id="mntStatus${mainList.contractNo}"><em>${mainList.mntStatusName}</em></span></td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
					<tr>
						<td colspan="13">데이터 없음</td>
					</tr>
			</c:otherwise>
		</c:choose>
					</tbody>
				</table>
			</div>
			<!-- //data list -->
			
			<!-- page_area -->
			<div class="page_area">
				<div class="btn_monitor">
					<button type="button" id="mntRunBtn" class="btn_default01" onclick="checkConfirm();" >모니터링 실행</button>
					<button type="button" class="btn_default04" onclick="allCallCancel();">일괄 취소</button>
				</div>
				
				<%@ include file="../common/paging.jsp" %>
			</div>
			<!-- //page_area -->

			<input type="hidden" id="autoYN" name="autoYN" />
			<input type="hidden" id="checkedChkBox" name="checkedChkBox" />
			<input type="hidden" id="checkedChkBoxLen" name="checkedChkBoxLen" value="${frontMntVO.checkedChkBoxLen}" />
			
			<input type="hidden" id="schCampId" name="schCampId" />
			<input type="hidden" id="schMntType" name="schMntType" />
			<input type="hidden" id="schOpNm" name="schOpNm" />
			<input type="hidden" id="schTargetDt" name="schTargetDt" />
			<input type="hidden" id="schFinalResult" name="schFinalResult" />
			<input type="hidden" id="schCallCnt" name="schCallCnt" />
			<input type="hidden" id="schCustNm" name="schCustNm" />
			<input type="hidden" id="schCustId" name="schCustId" />
			<input type="hidden" id="schCustTelNo" name="schCustTelNo" />
			<input type="hidden" id="schMemo" name="schMemo" />
			<input type="hidden" id="schCallState" name="schCallState" />
			<input type="hidden" id="schCallResult" name="schCallResult" />

			<input type="hidden" id="sortingTarget" name="sortingTarget" />
			<input type="hidden" id="direction" name="direction" />
			<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}"/>
			
			<input type="hidden" id="currentPage" name="currentPage" value="${paging.currentPage}" /> 

		</div>
		<!-- //contents -->
	</div>

	<%--// 각각의 알림창 관련 id--%>
	<div id="warnDialog" class="dialog"></div>
	<div id="checkKey1Dialog" class="dialog"></div>
	<div id="checkKey2Dialog" class="dialog"></div>
	<div id="checkKey3Dialog" class="dialog"></div>
	<!-- //container -->
	<!-- footer -->
	<div id="footer">
		
	</div>
	<!-- //footer -->
</div>
<!-- //wrap -->


<!-- javascript link & init -->
<%@ include file="../common/footer_init.jsp" %>
<!-- //javascript link & init -->

<script type="text/javascript">
var ws;												// 웹소켓 전역
var autoYN;											// 자동 모니터링 실행 여부.
var sortingTarget = "${frontMntVO.sortingTarget}";
var direction = "${frontMntVO.direction}";

$(document).ready(function() {
	autoYN = '<c:out value="${frontMntVO.autoYN}" />';

	conn_ws();
    getCallStatus();

    window.setTimeout(function () {
        if( autoYN == 'Y' ){
            init();
        }
    }, 500);

	$('#schTopCampId').val( '<c:out value="${frontMntVO.schCampId}" />' );
	$('#schTopMntType').val( '<c:out value="${frontMntVO.schMntType}" />' );
 	$('#schTopOpNm').val( '<c:out value="${frontMntVO.schOpNm}" />' );
 	$('#schTopTargetDt').val( '<c:out value="${frontMntVO.schTargetDt}" />' );
 	$('#schTopFinalResult').val( '<c:out value="${frontMntVO.schFinalResult}" />' );
 	$('#schTopCallCnt').val( '<c:out value="${frontMntVO.schCallCnt}" />' );
 	$('#schTopCustNm').val( '<c:out value="${frontMntVO.schCustNm}" />' );
 	$('#schTopCustId').val( '<c:out value="${frontMntVO.schCustId}" />' );
 	$('#schTopCustTelNo').val( '<c:out value="${frontMntVO.schCustTelNo}" />' );
 	$('#schTopMemo').val( '<c:out value="${frontMntVO.schMemo}" />' );
 	$('#schTopCallState').val( '<c:out value="${frontMntVO.schCallState}" />' );
 	$('#schTopCallResult').val( '<c:out value="${frontMntVO.schCallResult}" />' );

	$('#sortingTarget').val( '<c:out value="${frontMntVO.sortingTarget}" />' );
	$('#direction').val( '<c:out value="${frontMntVO.direction}" />' );

	if(sortingTarget) {
		initSort();
	}

	var warnDialogMsg = '데이터를 선택해주세요.';
	var warnDialogId = 'warnDialog';
	var warnDialogType = 'warn';
	setDialog(warnDialogId, warnDialogMsg, warnDialogType);

	var checkKey1DialogMsg = "숫자/영문/일부 특수기호('.', '-', '\\')만 입력 가능합니다.";
	var checkKey1DialogId = 'checkKey1Dialog';
	var checkKey1DialogType = 'checkKey1';
	setDialog(checkKey1DialogId, checkKey1DialogMsg, checkKey1DialogType);

	var checkKey2DialogMsg = "숫자만 입력 가능합니다.";
	var checkKey2DialogId = 'checkKey2Dialog';
	var checkKey2DialogType = 'checkKey2';
	setDialog(checkKey2DialogId, checkKey2DialogMsg, checkKey2DialogType);

	var checkKey3DialogMsg = "숫자/일부 특수기호('.', '-', '\\')만 입력 가능합니다.";
	var checkKey3DialogId = 'checkKey3Dialog';
	var checkKey3DialogType = 'checkKey3';
	setDialog(checkKey3DialogId, checkKey3DialogMsg, checkKey3DialogType);
});

function initSort() {
	var backup = $('#' + sortingTarget).html();
	backup = backup.substring(backup.indexOf('<i>')).trim();
	if(direction === 'asc') {
		$('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-down"></i>');
	} else if(direction === 'desc') {
		$('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-up"></i>');
	} else {
		$('#' + sortingTarget).html(backup);
	}
}

function onChangeSort(id) {
	if(sortingTarget !== id) {
		sortingTarget = id;
		direction = 'asc';
	} else {
		if(direction === 'asc') {
			direction = 'desc';
		} else if (direction === 'desc') {
			sortingTarget = '';
			direction = '';
		} else {
			direction = 'asc';
		}
	}
	$('#sortingTarget').val(sortingTarget);
	$('#direction').val(direction);

	//TODO: develop 브랜치로 merge 후 sorting에 대한 goSerch 매개변수 조건 추가해야함. / 검색시 sorting 관련 변수 초기화도 필요 [20190911 by Maro Kim]
	goSearch();
}

function setDialog(id, msg, type) {
	var buttons;
	if(type ==='warn') {
		buttons = [
			{
				text: "Cancel",
				click: function() {
					$(this).dialog("close");
				}
			}
		];
	} else {
		buttons = [
			{
				text: "Cancel",
				click: function () {
					$(this).dialog("close");
					location.reload();
				}
			}
		];
	}

	$('#'+id).html(msg);
	$('#'+id).dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		buttons: buttons
	});
}

function checkConfirm() {
	var checkedRow = $('input:checkbox[name=chkBoxes]:checked');
	if(checkedRow.length == 0) {
		$('#warnDialog').dialog('open');
	} else {
		runMonitoring();
	}
}

//자동 모니터링 실행 버튼 클릭 후 체크된 박스 컨트롤러로 전달 후, 체크된 것 상위로 정렬 시키면서 초기화.
async function init(){
	var checkedBox = '<c:out value="${frontMntVO.checkedChkBox}" />';
	var autoNo = checkedBox.split(",");
	var cno = "";
	var cust_tel_no = "";
	var cust_op_id = "";
	var camp_id = "";
	
		for( var one in autoNo ){
			$('#chkBox' + autoNo[one]).attr("checked", true);
		}

		for(var i=0; i<autoNo.length; i++){
			cno = autoNo[i];

			cust_tel_no = $('#hidden1' + cno).val();
			cust_op_id = $('#hidden2' + cno).val();
			camp_id = $('#hidden3' + cno).val();

	   		var sendMsg = '{"EventType":"STT", "Event":"START", "Caller":"' + cust_tel_no + '", "Agent":"' + cust_op_id + '", "contractNo":"' + cno + '", "campaignId":"' + camp_id + '"}';
 			sendMsgRestful(sendMsg);
		}
}



//체크 박스 전체 선택/해제
function chkBoxAllCheck(obj){
	if( obj.checked ){
		$('input[name=chkBoxes]').prop("checked", true);
	}else{
		$('input[name=chkBoxes]').prop("checked", false);
	}
}

//체크박스 개별 선택/해제
function chkBoxOneCheck(obj){
	if( obj.checked ){
		obj.checked = true;
	}else{
		obj.checked = false;
	}
}


//검색실행
function goSearch(condition){
	if(condition ==  true) {
		$('#currentPage').val(1);
	}

	var schCampId      = $('#schTopCampId').val();
	var schMntType     = $('#schTopMntType').val();
	var schOpNm        = $('#schTopOpNm').val();
	var schTargetDt    = $('#schTopTargetDt').val();
	var schFinalResult = $('#schTopFinalResult').val();
	var schCallCnt     = $('#schTopCallCnt').val();
	var schCustNm      = $('#schTopCustNm').val();
	var schCustId     = $('#schTopCustId').val();
	var schCustTelNo   = $('#schTopCustTelNo').val();
	var schMemo        = $('#schTopMemo').val();
	var schCallState   = $('#schTopCallState').val();
	var schCallResult  = $('#schTopCallResult').val();

	$('#schCampId').val(schCampId);
	$('#schMntType').val(schMntType);
	$('#schOpNm').val(schOpNm);
	$('#schTargetDt').val(schTargetDt);
	$('#schFinalResult').val(schFinalResult);
	$('#schCallCnt').val(schCallCnt);
	$('#schCustNm').val(schCustNm);
	$('#schCustId').val(schCustId);
	$('#schCustTelNo').val(schCustTelNo);
	$('#schMemo').val(schMemo);
	$('#schCallState').val(schCallState);
	$('#schCallResult').val(schCallResult);
	
	valueForm.method = "POST";
	valueForm.action = "/autoCallMnt";
	valueForm.submit();
}


//페이지 이동 관련
function goPage(cp){
	if( cp != 0){
		$('#currentPage').val(cp);
	}
	
	goSearch(false);
}


//자동 모니터링 팝업 실행
function runAutoPop(cno,call_id){
	var popSize = "width=800,height=650";
	var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no";
	var pop1 = window.open("/manualPop?pcnt=1&chk_cnt=1" + "&ctn=" + call_id + "&cno=" + cno + "&isCall=N","pop1", popSize + "," + popOption);
}


//자동 모니터링 실행.
function runMonitoring(){
	var checkedArr = [];
	$('input:checkbox[name=chkBoxes]:checked').each(function(){
		checkedArr.push($(this).val());
	});

	$('#checkedChkBox').val(checkedArr.toString());
	$('#autoYN').val('Y');

	var tmpLen = checkedArr.length;
	$('#checkedChkBoxLen').val( tmpLen );

	var popSize = "width=800,height=650";
	var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no";
	var pop1 = "";

	if( tmpLen == 1 ){
		pop1 = window.open("/manualPop?pcnt=1&chk_cnt=" + tmpLen + "&cno=" + checkedArr[0] + "&isCall=Y", "pop1", popSize + "," + popOption);

	}else if( tmpLen > 1 ){
		pop1 = window.open("/manualPop?pcnt=1&chk_cnt=" + tmpLen + "&cno=" + checkedArr[0] + "&isCall=Y", "pop1", popSize + "," + popOption);
	}

	valueForm.action = "/autoCallMnt";
	valueForm.method = "POST";
	valueForm.submit();

}

//자동 모니터링 실행 일괄 취소
async function allCallCancel(){
	var checkedArr = [];
	$('input:checkbox[name=chkBoxes]:checked').each(function(){
		checkedArr.push($(this).val());
	});

	for( one in checkedArr ) {
		cno = checkedArr[one];
		camp_id = $('#hidden3' + cno).val();

		sendMsg = '{"EventType":"STT", "Event":"STOP", "contractNo":"' + cno + '", "campaignId":"' + camp_id + '"}';
		sendMsgRestful(sendMsg);
	}

	$('input:checkbox[name=chkBoxes]:checked').each(function(){
		$(this).attr("checked", false);
	});
	$('#chkBox').attr("checked", false);
	$('#checkedChkBox').val('');
	$('#mntRunBtn').attr("disabled", false);
}


//websocket main function
function conn_ws(){

	ws = new WebSocket( '${websocketUrl}/callsocket');

	ws.onmessage = function(e){
		var rcv_data = JSON.parse(e.data);

		if( rcv_data.EventType == 'CALL' ){

			if( rcv_data.Event == 'status' ){
			}else if( rcv_data.Event == 'mntresult' ){		//모니터링 내용
				makeHtmlMntStatus(rcv_data);
			}
			getCallStatus();

		}else if( rcv_data.EventType == 'STT' ){
			if(rcv_data.Direction == 'TX'){
				makeMsgTX(rcv_data)				
			}else if(rcv_data.Direction == 'RX'){
				makeMsgRX(rcv_data)
			}
			
		}else if( rcv_data.EventType == 'DETECT' ){
			
			if( getd_contract_no == rcv_data.contract_no ){
				if( rcv_data.Result == 'Y' ){
					$('#resultYes' + rcv_data.No).focus();
					$('#resultYes' + rcv_data.No).prop("checked", true);
				}else{
					$('#resultNo' + rcv_data.No).focus();
					$('#resultNo' + rcv_data.No).prop("checked", true);
				}
			}
		}
	};
  	ws.onopen = function(){
  		var sendMsg = '{"EventType":"CALL", "Event":"subscribe"}';
   		ws.send(sendMsg);
  	}
}

//통화연결 restful
function sendMsgRestful(sendMsgStr){
	sendUrl = "/sendCM";
	
	$.ajax({
		url : sendUrl,
		data : {sendMsgStr:sendMsgStr},
		type : "POST",
		beforeSend : function(xhr) {
			/*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
			xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
		},
	}).done(function(data){
		console.log("### SUCC : " + data);
	}).fail(function(data){
		console.log("### FAIL : " + data);
	});
}

//모니터링 내용 실시간 반영
function makeHtmlMntStatus(rcv_data){
	var code = rcv_data.call_status;
	
	if( code == 'MR0001' ){
		$('#mntStatus' + rcv_data.contract_no).html( '<em>전체완료</em>' );
	}else if( code == 'MR0002' ){
		$('#mntStatus' + rcv_data.contract_no).html( '<em>통화거부</em>' );
	}else if( code == 'MR0003' ){
		$('#mntStatus' + rcv_data.contract_no).html( '<em>통화중</em>' );
	}else if( code == 'MR0004' ){
		$('#mntStatus' + rcv_data.contract_no).html( '<em>콜백요청</em>' );
	}else{
		if( code.length > 6 ){
			$('#mntStatus' + rcv_data.contract_no).html( '<em>' + rcv_data.taskNo_name + '</em>' );
		}
	}
}

function getCallStatus() {
	//TODO: 최근 통화리스트도 api로 구현되어 이곳에서 업데이트 되어야함. By Maro 2019.06.19
	var protocol = location.protocol + '//';
	var host = location.host;
	var suffix = '/api/outboundCallStatus';
	var url = protocol + host + suffix;

	$.get(url, function (result) {
		if (result) {
			$('#sumIng').html(0);
			$('#sumTransfer').html(0);
			$('#sumErr').html(0);
			$('#sumDone').html(0);
			$('#sumWait').html(0);
			$('#sumAll').html(0);
			$('#sumEtc').html(0);

			var sum = 0;
			var sumEtc = 0;
			result.forEach(function (item, idx) {
				sum += item['count'];
				if (item['call_status'] === 'CS0002') {
					// AI 진행중
					$('#sumIng').html(item['count']);
				} else if (item['call_status'] === 'CS0009') {
					// 상담사진행중
					$('#sumTransfer').html(item['count']);
				} else if (item['call_status'] === 'CS0003') {
					// 중지
					$('#sumErr').html(item['count']);
				} else if (item['call_status'] === 'CS0005') {
					// AI 종료
					$('#sumDone').html(item['count']);
				} else if (item['call_status'] === 'CS0008') {
					// 콜대기중
					$('#sumWait').html(item['count']);
				} else {
				    sumEtc += item['count'];
                }

				if (idx === result.length - 1) {
					$('#sumAll').html(sum);
                    $('#sumEtc').html(sumEtc);
				}
			});

		} else {
			// 데이터 획득 실패
			return;
		}
	})
}


//키 입력시 허용 값 체크
function checkKey( objName ){
	if( objName == 'campId' ){
		if( check_key_reg(1, $('#schTopCampId').val() ) == false ){
			$('#schTopCampId').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'targetDt' ){
		if( check_key_reg(3, $('#schTopTargetDt').val() ) == false ){
			$('#schTopTargetDt').val('');
			$('#checkKey3Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'callCnt' ){
		if( check_key_reg(2, $('#schTopCallCnt').val() ) == false ){
			$('#schTopCallCnt').val('');
			$('#checkKey2Dialog').dialog('open');
			return false;
		}
	}else if( objName == 'custId' ){
		if( check_key_reg(1, $('#schTopCustId').val() ) == false ){
			$('#schTopCustId').val('');
			$('#checkKey1Dialog').dialog('open');
			return false;
		}		
	}else if( objName == 'custTelNo' ){
		if( check_key_reg(3, $('#schTopCustTelNo').val() ) == false ){
			$('#schTopCustTelNo').val('');
			$('#checkKey3Dialog').dialog('open');
			return false;
		}
	}
}

//값 체크 정규식 함수
function check_key_reg( mode, text ){
	if( mode == 1 ){
	    var regexp = /[0-9a-zA-Z.\-\\/]/; 	//숫자,영문,특수문자 허용
	    
	}else if( mode == 2 ){
		var regexp = /[0-9]/; 				// 숫자만 허용
		
	}else if( mode == 3 ){
		var regexp = /[0-9.\-\\/]/; 		// 숫자, '. - \ /'만 허용
		
	}else if( mode == 4 ){
		var regexp = /[a-zA-Z]/; 			// 영문만 허용
		
	}
    
    for( var i=0; i<text.length; i++){
        if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false ){

			return false;
        }
    }  
}

//엔터값 체크
function enterCheck(){
	if( window.event.keyCode == 13 ){
		goSearch(true);
	}else{
		return false;
	}
}
</script>

</form>

</body>
</html>
