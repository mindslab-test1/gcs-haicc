<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <%@ include file="../common/header_headLink.jsp" %>

    <title>Happy Call</title>
</head>
<body>

<form id="valueForm" name="valueForm">

    <!-- .page loading -->
    <div id="page_ldWrap" class="page_loading"><div class="loading_itemBox"><span></span><span></span><span></span><span></span></div></div>
    <!-- //.page loading -->

    <!-- wrap -->
    <div id="wrap">
        <!-- header -->
        <%@ include file="../common/header_headProfile.jsp" %>
        <!-- //header -->

        <!-- container -->
        <div id="container">
            <!-- snb -->
            <%@ include file="../common/leftMenu2.jsp" %>
            <!-- //snb -->

            <!-- contents -->
            <div id="contents">

                <!-- header search -->
                <%@ include file="../common/inboundHeaderSearch.jsp" %>

                <!-- data list -->
                <div class="data_list tbl_thead">
                    <table>
                        <colgroup>
                            <col style="width:45px;"><col style="width:auto;"><col style="width:10%;">
                            <col style="width:10%;"><col style="width:10%;"><col style="width:10%;"><col style="width:10%;">
                            <col style="width:10%;"><col style="width:10%;"><col style="width:12%;">
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope="col">No.</th>
                            <th id='CAMPAIGN_NM' scope="col" onclick="onChangeSort(this.id)">모니터링 종류</th>
                            <th id='CUST_NM' scope="col" onclick="onChangeSort(this.id)">고객명</th>
                            <th id='CUST_ID' scope="col" onclick="onChangeSort(this.id)">고객관리번호</th>
                            <th id='CUST_TEL_NO' scope="col" onclick="onChangeSort(this.id)">전화번호</th>
                            <th id='DURATION' scope="col" onclick="onChangeSort(this.id)">통화길이</th>
                            <th id='LOAN_PRICE' scope="col" onclick="onChangeSort(this.id)">대출금액</th>
                            <th id='CALL_DATE' scope="col" onclick="onChangeSort(this.id)">통화일시</th>
                            <th id='CALL_STATUS_NM' scope="col" onclick="onChangeSort(this.id)">콜상태</th>
                            <th id='MNT_STATUS_NAME' scope="col" onclick="onChangeSort(this.id)">모니터링 내용</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="data_list tbl_tbody">
                    <table>
                        <colgroup>
                            <col style="width:45px;"><col style="width:auto;"><col style="width:10%;">
                            <col style="width:10%;"><col style="width:10%;"><col style="width:10%;"><col style="width:10%;">
                            <col style="width:10%;"><col style="width:10%;"><col style="width:12%;">
                        </colgroup>
                        <tbody>
                        <c:choose>
                            <c:when test="${fn:length(list) gt 0}">
                                <c:forEach items="${list}" var="list" varStatus="status">
                                    <tr style="cursor: pointer;" onclick="resultDetail(${list.contractNo},${list.lastCallId});">
                                        <td>${(frontMntVO.startRow + status.index + 1)}</td>
                                        <td>${list.campaignNm}</td>
                                        <td>${list.custNm}</td>
                                        <td>${list.custId}</td>
                                        <td>${list.telNo}</td>
                                        <td>${list.duration}</td>
                                        <td>${list.loanPrice}</td>
                                        <td>${list.callDate}</td>
                                        <td><i class="fas fa-circle done"></i><em>${list.callStatusNm}</em></td>
                                        <td><em>${list.mntStatusName}</em></td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td colspan="13">데이터 없음</td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                        </tbody>
                    </table>
                </div>
                <!-- //data list -->

                <!-- page_area -->
                <div class="page_area">
                    <%@ include file="../common/paging.jsp" %>
                </div>
                <!-- //page_area -->


                <input type="hidden" name="checkedChkBox" id="checkedChkBox" />
                <input type="hidden" id="schCampId" name="schCampId" />
                <input type="hidden" id="schMntType" name="schMntType" />
                <input type="hidden" id="schOpNm" name="schOpNm" />
                <input type="hidden" id="schTargetDt" name="schTargetDt" />
                <input type="hidden" id="schFinalResult" name="schFinalResult" />
                <input type="hidden" id="schCallCnt" name="schCallCnt" />
                <input type="hidden" id="schCustNm" name="schCustNm" />
                <input type="hidden" id="schCustId" name="schCustId" />
                <input type="hidden" id="schCustTelNo" name="schCustTelNo" />
                <input type="hidden" id="schMemo" name="schMemo" />
                <input type="hidden" id="schCallState" name="schCallState" />
                <input type="hidden" id="schCallResult" name="schCallResult" />

                <input type="hidden" id="sortingTarget" name="sortingTarget" />
                <input type="hidden" id="direction" name="direction" />
                <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}"/>

                <input type="hidden" id="currentPage" name="currentPage" value="${paging.currentPage}" />

            </div>
            <!-- //contents -->
        </div>

        <%--// 각각의 알림창 관련 id--%>
        <div id="checkKey1Dialog" class="dialog"></div>
        <div id="checkKey3Dialog" class="dialog"></div>
        <!-- //container -->
        <!-- footer -->
        <div id="footer">

        </div>
        <!-- //footer -->
    </div>
    <!-- //wrap -->


    <!-- javascript link & init -->
    <%@ include file="../common/footer_init.jsp" %>
    <!-- //javascript link & init -->

    <script type="text/javascript">
        var sortingTarget = "${frontMntVO.sortingTarget}";
        var direction = "${frontMntVO.direction}";

        $(document).ready(function(){
            var rows = '<c:out value="${frontMntVO.pageInitPerPage}" />';
            if( rows != '' ){
                $('#pageInitPerPage').val(rows);
            }

            $('#schTopCampId').val( '<c:out value="${frontMntVO.schCampId}" />' );
            $('#schTopMntType').val( '<c:out value="${frontMntVO.schMntType}" />' );
            $('#schTopOpNm').val( '<c:out value="${frontMntVO.schOpNm}" />' );
            $('#schTopTargetDt').val( '<c:out value="${frontMntVO.schTargetDt}" />' );
            $('#schTopFinalResult').val( '<c:out value="${frontMntVO.schFinalResult}" />' );
            $('#schTopCallCnt').val( '<c:out value="${frontMntVO.schCallCnt}" />' );
            $('#schTopCustNm').val( '<c:out value="${frontMntVO.schCustNm}" />' );
            $('#schTopCustId').val( '<c:out value="${frontMntVO.schCustId}" />' );
            $('#schTopCustTelNo').val( '<c:out value="${frontMntVO.schCustTelNo}" />' );
            $('#schTopMemo').val( '<c:out value="${frontMntVO.schMemo}" />' );
            $('#schTopCallState').val( '<c:out value="${frontMntVO.schCallState}" />' );
            $('#schTopCallResult').val( '<c:out value="${frontMntVO.schCallResult}" />' );

            $('#sortingTarget').val( '<c:out value="${frontMntVO.sortingTarget}" />' );
            $('#direction').val( '<c:out value="${frontMntVO.direction}" />' );

            if(sortingTarget) {
                initSort();
            }

            var checkKey1DialogMsg = "숫자/영문/일부 특수기호('.', '-', '\\')만 입력 가능합니다.";
            var checkKey1DialogId = 'checkKey1Dialog';
            var checkKey1DialogType = 'checkKey1';
            setDialog(checkKey1DialogId, checkKey1DialogMsg, checkKey1DialogType);

            var checkKey3DialogMsg = "숫자/일부 특수기호('.', '-', '\\')만 입력 가능합니다.";
            var checkKey3DialogId = 'checkKey3Dialog';
            var checkKey3DialogType = 'checkKey3';
            setDialog(checkKey3DialogId, checkKey3DialogMsg, checkKey3DialogType);
        });

        function initSort() {
            var backup = $('#' + sortingTarget).html();
            backup = backup.substring(backup.indexOf('<i>')).trim();
            if(direction === 'asc') {
                $('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-down"></i>');
            } else if(direction === 'desc') {
                $('#' + sortingTarget).html(backup + ' <i class="fas fa-long-arrow-alt-up"></i>');
            } else {
                $('#' + sortingTarget).html(backup);
            }
        }

        function onChangeSort(id) {
            if(sortingTarget !== id) {
                sortingTarget = id;
                direction = 'asc';
            } else {
                if(direction === 'asc') {
                    direction = 'desc';
                } else if (direction === 'desc') {
                    sortingTarget = '';
                    direction = '';
                } else {
                    direction = 'asc';
                }
            }
            $('#sortingTarget').val(sortingTarget);
            $('#direction').val(direction);

            //TODO: develop 브랜치로 merge 후 sorting에 대한 goSerch 매개변수 조건 추가해야함. / 검색시 sorting 관련 변수 초기화도 필요 [20190911 by Maro Kim]
            goSearch();
        }


        function setDialog(id, msg, type) {
            var buttons;
            if(type === 'checkKey1' || type === 'checkKey3'){
                buttons = [
                    {
                        text: "Cancel",
                        click: function () {
                            $(this).dialog("close");
                            location.reload();
                        }
                    }
                ];
            }

            $('#'+id).html(msg);
            $('#'+id).dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                buttons: buttons
            });
        }

        //검색실행
        function goSearch(condition){
            if(condition ==  true) {
                $('#currentPage').val(1);
            }

            var schCampId      = $('#schTopCampId').val();
            var schMntType     = $('#schTopMntType').val();
            var schOpNm        = $('#schTopOpNm').val();
            var schTargetDt    = $('#schTopTargetDt').val();
            var schFinalResult = $('#schTopFinalResult').val();
            var schCallCnt     = $('#schTopCallCnt').val();
            var schCustNm      = $('#schTopCustNm').val();
            var schCustId     = $('#schTopCustId').val();
            var schCustTelNo   = $('#schTopCustTelNo').val();
            var schMemo        = $('#schTopMemo').val();
            var schCallState   = $('#schTopCallState').val();
            var schCallResult  = $('#schTopCallResult').val();

            $('#schCampId').val(schCampId);
            $('#schMntType').val(schMntType);
            $('#schOpNm').val(schOpNm);
            $('#schTargetDt').val(schTargetDt);
            $('#schFinalResult').val(schFinalResult);
            $('#schCallCnt').val(schCallCnt);
            $('#schCustNm').val(schCustNm);
            $('#schCustId').val(schCustId);
            $('#schCustTelNo').val(schCustTelNo);
            $('#schMemo').val(schMemo);
            $('#schCallState').val(schCallState);
            $('#schCallResult').val(schCallResult);

            valueForm.method = "POST";
            valueForm.action = "/ibMntResult";
            valueForm.submit();
        }

        //페이지 이동 관련
        function goPage(cp){
            $('#currentPage').val(cp);

           goSearch(false);
        }

        function resultDetail(contract_no, call_id){
            console.log("call_id: "+call_id);
            var popSize = "width=800,height=650";
            var popOption = "titlebar=no,toolbar=no,menubar=no,location=no,directories=no,status=no,scrollbars=no";
            winpop1 = window.open("/ibMntResultPop?ctn="+call_id+"&cno="+contract_no,"winpop1", popSize + "," + popOption);

        }

        //키 입력시 허용 값 체크
        function checkKey( objName ){
            if( objName == 'campId' ){
                if( check_key_reg(1, $('#schTopCampId').val() ) == false ){
                    $('#schTopCampId').val('');
                    $('#checkKey1Dialog').dialog('open');
                    return false;
                }
            }else if( objName == 'targetDt' ){
                if( check_key_reg(1, $('#schTopTargetDt').val() ) == false ){
                    $('#schTopTargetDt').val('');
                    $('#checkKey1Dialog').dialog('open');
                    return false;
                }
            }else if( objName == 'callCnt' ){
                if( check_key_reg(2, $('#schTopCallCnt').val() ) == false ){
                    $('#schTopCallCnt').val('');
                    $('#checkKey3Dialog').dialog('open');
                    return false;
                }
            }else if( objName == 'custId' ){
                if( check_key_reg(1, $('#schTopCustId').val() ) == false ){
                    $('#schTopCustId').val('');
                    $('#checkKey1Dialog').dialog('open');
                    return false;
                }
            }else if( objName == 'custTelNo' ){
                if( check_key_reg(1, $('#schTopCustTelNo').val() ) == false ){
                    $('#schTopCustTelNo').val('');
                    $('#checkKey1Dialog').dialog('open');
                    return false;
                }
            }
        }

        //값 체크 정규식 함수
        function check_key_reg( mode, text ){
            if( mode == 1 ){
                var regexp = /[0-9a-zA-Z.\-\\/]/; 	//숫자,영문,특수문자 허용

            }else if( mode == 2 ){
                var regexp = /[0-9]/; 				// 숫자만 허용

            }else if( mode == 2 ){
                var regexp = /[0-9.\-\\/]/; 		// 숫자, '. - \ /'만 허용

            }else if( mode == 4 ){
                var regexp = /[a-zA-Z]/; 			// 영문만 허용

            }

            for( var i=0; i<text.length; i++){
                if(text.charAt(i) != " " && regexp.test(text.charAt(i)) == false ){
                    return false;
                }
            }
        }

        //엔터값 체크
        function enterCheck(){
            if( window.event.keyCode == 13 ){
                goSearch(true);
            }else{
                return false;
            }
        }
    </script>

</form>

</body>

</html>










