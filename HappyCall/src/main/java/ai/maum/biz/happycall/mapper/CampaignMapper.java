package ai.maum.biz.happycall.mapper;

import ai.maum.biz.happycall.models.dto.CmCampaignInfoDTO;
import ai.maum.biz.happycall.models.vo.MntTargetMngVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CampaignMapper {

    List<CmCampaignInfoDTO> getCampaignTaskList(int campaignId);

    List<CmCampaignInfoDTO> getCampaignListByService(String serviceName);

    int createServiceMonitoring(MntTargetMngVO mntTargetMngVO);

    int insertUser(MntTargetMngVO mntTargetMngVO);
}
