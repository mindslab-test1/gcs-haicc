package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.CommonMonitoringMapper;
import ai.maum.biz.happycall.models.dto.CallStatusDTO;
import ai.maum.biz.happycall.models.dto.CmCampaignInfoDTO;
import ai.maum.biz.happycall.models.dto.CmCampaignScoreDTO;
import ai.maum.biz.happycall.models.dto.CmSttResultDetailDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonMonitoringServiceImpl implements CommonMonitoringService {

    @Autowired
    CommonMonitoringMapper commonMonitoringMapper;

    @Override
    public List<CallStatusDTO> getCountOfCall(FrontMntVO frontMntVO) {
        return commonMonitoringMapper.getCountOfCall(frontMntVO);
    }

    @Override
    public String getRecentContractMemo(FrontMntVO frontMntVO) {
        return commonMonitoringMapper.getRecentContractMemo(frontMntVO);
    }

    @Override
    public List<CmCampaignInfoDTO> getCallPopMonitoringResultList(FrontMntVO frontMntVO) {
        return commonMonitoringMapper.getCallPopMonitoringResultList(frontMntVO);
    }

    @Override
    public List<CmCampaignScoreDTO> getScoreList(FrontMntVO frontMntVO) {
        return commonMonitoringMapper.getScoreList(frontMntVO);
    }

    @Override
    public List<CmSttResultDetailDTO> getSttResultAllList(FrontMntVO frontMntVO) {
        return commonMonitoringMapper.getSttResultAllList(frontMntVO);
    }
}
