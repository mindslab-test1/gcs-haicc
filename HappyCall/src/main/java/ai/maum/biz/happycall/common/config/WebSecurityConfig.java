package ai.maum.biz.happycall.common.config;

import ai.maum.biz.happycall.common.AuthProvider;
import ai.maum.biz.happycall.common.security.SuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
public class WebSecurityConfig {

    @Configuration
    static class Security extends WebSecurityConfigurerAdapter {
        @Autowired
        AuthProvider authProvider;

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            http
                .authorizeRequests()
                    .antMatchers(  "/login","/service","/service/**","/resources/**").permitAll()
//                    .antMatchers(
//                    "/mntTargetUpload",
//                            "/opAssign",
//                            "/manualCallMnt",
//                            "/autoCallMnt",
//                            "/mntResult"
//                            ).hasRole("ADMIN")
//                    .antMatchers("/ibMntResult", "/callStatus", "/userManage", "/callBackManage").hasAnyRole("ADMIN", "HH")
                    .antMatchers("/**").authenticated()
                    .and()
                .headers()
                    .frameOptions().disable()
                    .and()
                .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/loginProcess")
                    .successHandler(successHandler())
                    .permitAll()
                    .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/")
                    .invalidateHttpSession(true)
                    .and()
                .csrf()
                    .csrfTokenRepository(new CookieCsrfTokenRepository())
                    .and()
                .authenticationProvider(authProvider);
        }

        @Bean
        public PasswordEncoder passwordEncoder(){
            return PasswordEncoderFactories.createDelegatingPasswordEncoder();
        }

        @Bean
        public SuccessHandler successHandler(){
            return new SuccessHandler();
        }
    }
}
