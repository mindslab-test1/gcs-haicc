package ai.maum.biz.happycall.models.dto;

import ai.maum.biz.happycall.models.vo.TracingVO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Getter
@Setter
public class CmAuthUserRoleMapDTO{
    private int authUserRoleMapId;
    private String username;
    private String roleId;

    private int creatorId;
    private int updaterId;
    private String createdDtm;
    private String updatedDtm;
}
