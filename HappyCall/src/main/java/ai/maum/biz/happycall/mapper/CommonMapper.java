package ai.maum.biz.happycall.mapper;

import ai.maum.biz.happycall.models.dto.CmCommonCdDTO;
import ai.maum.biz.happycall.models.dto.CmOpInfoDTO;
import ai.maum.biz.happycall.models.vo.CmCommonCdVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CommonMapper {
	
	public List<CmCommonCdDTO> getSeachCodeList();
}
