package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.CampaignMapper;
import ai.maum.biz.happycall.models.dto.CmCampaignInfoDTO;
import ai.maum.biz.happycall.models.vo.MntTargetMngVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CampaignServiceImpl implements CampaignService {

    @Autowired
    CampaignMapper campaignMapper;

    @Override
    public List<CmCampaignInfoDTO> getCampaignTaskList(int campaignId) {
        return campaignMapper.getCampaignTaskList(campaignId);
    }

    @Override
    public List<CmCampaignInfoDTO> getCampaignListByService(String serviceName) {
        return campaignMapper.getCampaignListByService(serviceName);
    }

    @Transactional
    @Override
    public int createServiceMonitoring(MntTargetMngVO mntTargetMngVO) {
        int custId = campaignMapper.insertUser(mntTargetMngVO);

        mntTargetMngVO.setCustId(custId);

        return campaignMapper.createServiceMonitoring(mntTargetMngVO);
    }
}
