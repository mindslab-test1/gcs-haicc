package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CallHistoryDTO {
	private String callId;
	private String callDate;
	private String callTypeCode;
	private String contractNo;
	private String startTime;
	private String endTime;
	private String duration;
	private String callStatus;
	private String campStatus;
	private String createDtm;
	private String callMemo;
	private String monitorCont;
	private String callbackDt;
	private String mntStatus;
	private String mntStatusName;
	
	private String callStatusNm;
	private String cdMntStatus;
	private String chCallStatusNm;
}
