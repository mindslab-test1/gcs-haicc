package ai.maum.biz.happycall.models.vo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

@Data
public class AuthVO {

    private List<String> usernameList; //for delete
    private int userId;
    private String username;
    private String name;
    private String password;
    private String newPassword;        //for update password
    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;
    private Collection<? extends GrantedAuthority> authorities;
    private String targetDt;           //created_date
    private List<String> checkedList;
    private List<String> delAuthList;
    private List<String> accountCheckedList;

    private TracingVO tracingVO = new TracingVO();
}
