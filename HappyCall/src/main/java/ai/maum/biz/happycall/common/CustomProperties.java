package ai.maum.biz.happycall.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class CustomProperties {
	@Value("${websocket.protocol}")
	String websocketProtocol;

	@Value("${websocket.ip}")
	String websocketIp;
	
	@Value("${websocket.port}")
	String websocketPort;
	
	@Value("${audio.ip}")
	String audioIp;
	
	@Value("${audio.port}")
	String audioPort;

	@Value("${rest.ip}")
	String restIp;

	@Value("${rest.port}")
	String restPort;

	@Value("${excel.form.path}")
	String excelFormPath;

	@Value("${excel.upload.path}")
	String excelUploadPath;

	public String getWebsocketProtocol() {
		return websocketProtocol;
	}

	public String getWebsocketIp() {
		return websocketIp;
	}

	public String getWebsocketPort() {
		return websocketPort;
	}

	public String getAudioIp() {
		return audioIp;
	}

	public String getAudioPort() {
		return audioPort;
	}

	public String getRestIp() {
		return restIp;
	}

	public String getRestPort() {
		return restPort;
	}

	public String getExcelFormPath() {
		return excelFormPath;
	}

	public String getExcelUploadPath() {
		return excelUploadPath;
	}

}
