package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CmSttResultDetailDTO {
	private String sttResultDetailId;
	private String sttResultId;
	private String callId;
	private String speakerCode;
	private String sentenceId;
	private String sentence;
	private String startTime;
	private String endTime;
	private String speed;
	private String silenceYn;
	private String ignored;
	private String createdDtm;
	private String updatedDtm;
	private String creatorId;
	private String updatorId;
}
