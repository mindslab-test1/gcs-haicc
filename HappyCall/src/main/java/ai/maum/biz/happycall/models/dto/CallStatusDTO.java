package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CallStatusDTO {
    private String callStatus;
    private int count;
}
