package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.AuthMapper;
import ai.maum.biz.happycall.models.dto.CmAuthUserDTO;
import ai.maum.biz.happycall.models.dto.CmAuthRoleDTO;
import ai.maum.biz.happycall.models.vo.AuthVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    AuthMapper authMapper;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        CmAuthUserDTO account = authMapper.getAccount(userId);
        account.setAuthorities(getAuthorities(userId));

        return account;
    }

    public Collection<GrantedAuthority> getAuthorities(String username) {
        List<String> string_authorities = authMapper.getAuthorities(username);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String authority : string_authorities) {
            authorities.add(new SimpleGrantedAuthority(authority));
        }
        return authorities;
    }

    public CmAuthUserDTO getAccount(String userId) {
        return authMapper.getAccount(userId);
    }

    //Role List
    public List<CmAuthRoleDTO> getRoleList() {
        return authMapper.getRoleList();
    }

    public List<CmAuthUserDTO> getUserList(FrontMntVO frontMntVO){
        return authMapper.getUserList(frontMntVO);
    }

    //사용자 추가할 데이터
    @Transactional("transactionManager")
    public int addAccount(AuthVO authVO) {
        authVO.setPassword(passwordEncoder.encode(authVO.getPassword()));
        authVO.setAccountNonExpired(true);
        authVO.setAccountNonLocked(true);
        authVO.setCredentialsNonExpired(true);
        authVO.setEnabled(true);
        authMapper.insertUser(authVO);
        return authMapper.insertAuth(authVO);
    }

    // 계정 중복 확인
    public int checkDup(AuthVO authVO) {
        return authMapper.checkDup(authVO);
    }

    //Id와 일치하는 계정 정보 가져오기
    public CmAuthUserDTO getAccountById(int id) {
        return authMapper.getAccountById(id);
    }

    //paging
    public int getresultUserTotalCount(FrontMntVO frontMntVO) { return authMapper.getresultUserTotalCount(); }

    //기존 계정 수정 데이터
    @Override
    @Transactional("transactionManager")
    public int updateAccount(AuthVO authVO) {
        authVO.setNewPassword(passwordEncoder.encode(authVO.getNewPassword()));
        //수정시 권한 추가하는 경우
        if(authVO.getCheckedList() != null) {
           authMapper.insertAuth(authVO);
        }
        //수정시 권한 삭제하는 경우
        if(authVO.getDelAuthList() != null) {
           authMapper.delAuth(authVO);
        }
        return authMapper.updateAccount(authVO);
    }

    //기존 계정중 체크된 거 비활성화할 계정
    @Override
    public int disableAccount(AuthVO authVO) { return authMapper.disableAccount(authVO);
    }

    //기존 계정중 체크된 거 활성화할 계정
    @Override
    public int enableAccount(AuthVO authVO) { return authMapper.enableAccount(authVO);
    }

    //기존 계정별 Auth List 데이터
    public List<CmAuthRoleDTO> getAuthList(String username) { return authMapper.getAuthList(username); }

}
