package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.common.util.Utils;
import ai.maum.biz.happycall.common.util.VariablesMng;
import ai.maum.biz.happycall.mapper.OutboundMonitoringMapper;
import ai.maum.biz.happycall.models.dto.CallHistoryDTO;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class OutboundMonitoringServiceImpl implements OutboundMonitoringService {

    @Autowired
    OutboundMonitoringMapper outboundMonitoringMapper;

    @Autowired
    VariablesMng variablesMng;

    @Autowired
    Utils utils;

    @Override
    public List<CmContractDTO> getOutboundCallMntList(FrontMntVO frontMntVO) throws ParseException {
        List<CmContractDTO> result = outboundMonitoringMapper.getOutboundCallMntList(frontMntVO);
        final int basePeriod = variablesMng.getBasePeriod(); // 기준일자
        final int callCount = variablesMng.getCallCount(); // 콜 횟수
        final SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");

        for(int i=0; i<result.size(); i++) {
            List<CallHistoryDTO> callHistoryDTOList = result.get(i).getCallHistDTO();
            CmContractDTO mntrTrgtMngmtDTO = result.get(i);
            String referDateStr = mntrTrgtMngmtDTO.getTargetDt();

            if(!StringUtils.isNotBlank(referDateStr)) {
                // 배정일자가 할당되지 않았을 때
                mntrTrgtMngmtDTO.setFinalResult("미실행");
                continue;
            }

            Date referDate = transFormat.parse(referDateStr);
            Date today = new Date();

            long referDiff = utils.getDateDiff(today, referDate);
            int callTryCount = mntrTrgtMngmtDTO.getCallTryCount();

            if(referDiff > basePeriod || callTryCount > callCount) {
                // 실행조건(콜카운트,기준일자)를 만족하지 않음
                if(callHistoryDTOList.size() == 0) {
                    // 실행조건 만족하지 않고, 전화가 한번도 실행되지 않은 경우
                    mntrTrgtMngmtDTO.setFinalResult("불완전판매");
                    continue;
                }

                for(int j = 0; j< callHistoryDTOList.size(); j++){
                    CallHistoryDTO callHistoryDTO = callHistoryDTOList.get(j);
                    String callDateStr = callHistoryDTO.getCallDate();
                    Date callDate = transFormat.parse(callDateStr);

                    long callDiff = utils.getDateDiff(callDate, referDate);
                    String mntCode = callHistoryDTO.getMntStatus();
                    if(!StringUtils.isNotBlank(mntCode)) {
                        //모니터링 코드가 할당되지 않은 경우
                    } else if(callDiff < basePeriod && mntCode.equals("MR0001")) {
                        // 기간안에 완료가 있을 경우 완전판매
                        mntrTrgtMngmtDTO.setFinalResult("완전판매");
                        break;
                    }

                    if(j == callHistoryDTOList.size() - 1) {
                        mntrTrgtMngmtDTO.setFinalResult("불완전판매");
                    }
                }
            } else {
                // 실행조건(콜카운트,기준일자)를 만족
                if(callHistoryDTOList.size() == 0) {
                    mntrTrgtMngmtDTO.setFinalResult("미실행");
                    continue;
                }
                for(int j = 0; j< callHistoryDTOList.size(); j++) {
                    CallHistoryDTO callHistoryDTO = callHistoryDTOList.get(j);
                    String mntCode = callHistoryDTO.getMntStatus();

                    if(!StringUtils.isNotBlank(mntCode)) {
                        //모니터링 코드가 할당되지 않은 경우
                    } else if(mntCode.equals("MR0001")) {
                        mntrTrgtMngmtDTO.setFinalResult("완전판매");
                        break;
                    }

                    if(j == callHistoryDTOList.size() - 1) {
                        mntrTrgtMngmtDTO.setFinalResult("진행중");
                    }
                }
            }
        }
        return result;
    }

    @Override
    public int getOutboundCallMntCount(FrontMntVO frontMntVO) {
        return outboundMonitoringMapper.getOutboundCallMntCount(frontMntVO);
    }

    @Override
    public CmContractDTO getOutboundCallMntData(FrontMntVO frontMntVO) {
        return outboundMonitoringMapper.getOutboundCallMntData(frontMntVO);
    }

    @Override
    public int updateMemo(FrontMntVO frontMntVO) {
        return outboundMonitoringMapper.updateMemo(frontMntVO);
    }

    @Override
    public List<String> getCallHistList(FrontMntVO frontMntVO) {
        return outboundMonitoringMapper.getCallHistList(frontMntVO);
    }
}
