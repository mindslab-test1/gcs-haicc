package ai.maum.biz.happycall.controller;

import ai.maum.biz.happycall.models.dto.CallStatusDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import ai.maum.biz.happycall.service.CommonMonitoringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ApiController {

    @Autowired
    CommonMonitoringService commonMonitoringService;

    /*  인바운드 모니터링 */
    @RequestMapping(value="/inboundCallStatus", method = {RequestMethod.GET})
    public List<CallStatusDTO> inboundCallStatus() {
        FrontMntVO vo = new FrontMntVO();
        vo.setIsInbound("Y");
        List<CallStatusDTO> result = commonMonitoringService.getCountOfCall(vo);
        return result;
    }

    /*  아웃바운드 모니터링 */
    @RequestMapping(value="/outboundCallStatus", method = {RequestMethod.GET})
    public List<CallStatusDTO> outboundCallStatus() {
        FrontMntVO vo = new FrontMntVO();
        vo.setIsInbound("N");
        List<CallStatusDTO> result = commonMonitoringService.getCountOfCall(vo);
        return result;
    }
}
