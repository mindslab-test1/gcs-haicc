package ai.maum.biz.happycall.controller;

import ai.maum.biz.happycall.common.util.Utils;
import ai.maum.biz.happycall.common.util.VariablesMng;
import ai.maum.biz.happycall.models.dto.*;
import ai.maum.biz.happycall.models.vo.AuthVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import ai.maum.biz.happycall.models.vo.PagingVO;
import ai.maum.biz.happycall.service.AuthService;
import ai.maum.biz.happycall.service.CommonService;
import ch.qos.logback.classic.pattern.SyslogStartConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AuthController {

    @Autowired
    AuthService authService;

    @Autowired
    CommonService commonService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Inject
    Utils utils;

    @Inject
    VariablesMng variablesMng;

    @RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
    public String goLogin() {

        return "/login";
    }

    //로그인 - 로그인 버튼 클릭 시 호출, 로그인 체크.
    @RequestMapping(value = "/logout", method = {RequestMethod.GET, RequestMethod.POST})
    public String doLogout(HttpSession session) {
        utils.logout(session);

        return "/login";
    }

    @RequestMapping("/failed")
    @ResponseBody
    public String failed() {
        return "Failed";
    }

    /* 사용자 관리 리스트 페이지 */
    @RequestMapping(value = "/userManage", method = {RequestMethod.GET, RequestMethod.POST})
    public String userManage(Model model, FrontMntVO frontMntVO) {

        //페이징을 위해서 쿼리 포함 전체 카운팅
        PagingVO pagingVO = new PagingVO();
        pagingVO.setCOUNT_PER_PAGE(frontMntVO.getPageInitPerPage());
        pagingVO.setTotalCount(authService.getresultUserTotalCount(frontMntVO));
        pagingVO.setCurrentPage(frontMntVO.getCurrentPage());
        frontMntVO.setStartRow(pagingVO.getStartRow());
        frontMntVO.setLastRow(pagingVO.getLastRow());

        //Front에 전달할 객체들 생성.
        frontMntVO.setPageInitPerPage(String.valueOf(pagingVO.getCOUNT_PER_PAGE()));

        //userlist 조회
        List<CmAuthUserDTO> userList = authService.getUserList(frontMntVO);

        model.addAttribute("userList", userList);
        model.addAttribute("paging", pagingVO);
        model.addAttribute("menuId", variablesMng.getMenuIdString("userManage"));			       // menuId 설정.
        model.addAttribute("username", Utils.getLogInAccount(authService).getName());     // 로그인한 사용자 이름

        return "/monitoring/accountManage";
    }

    //사용자 추가 및 수정 시 팝업 띄우기, 전체 Role List, 선택한 계정 기존 정보 가져오기
    @RequestMapping(value = "/accountManagePop", method = RequestMethod.GET)
    public String accountManagePop(Model model, @RequestParam String popupType, AuthVO authVO) {

        if( popupType.equals("insert") ){
            List<CmAuthRoleDTO> roleList = authService.getRoleList();

            model.addAttribute("roleList", roleList);
            model.addAttribute("popupType", popupType);
        } else if( popupType.equals("modify") ) {
            CmAuthUserDTO account = authService.getAccountById(authVO.getUserId());
            List<CmAuthRoleDTO> roleList = authService.getRoleList();
            List<CmAuthRoleDTO> authList = authService.getAuthList(account.getUsername());

            model.addAttribute("userId", account.getUserId());
            model.addAttribute("username", account.getUsername());
            model.addAttribute("name", account.getName());
            model.addAttribute("roleList", roleList);
            model.addAttribute("authList", authList);
            model.addAttribute("popupType", popupType);
        }

        return "monitoring/accountManagePopup";
    }

    //생성자 추가 팝업창에서 입력값 저장
    @RequestMapping(value = "/addAccount", method = RequestMethod.POST)
    @ResponseBody
    public String addUser(AuthVO authVO) {

        int check = authService.checkDup(authVO);

        // 데이터 중복 체크
        if(check == 1) {
            return "DUP";
        } else {
            int result = authService.addAccount(authVO);

            if( result != 0 ) {
                return "SUCC";
            } else {
                return "FAIL";
            }
        }
    }

    //계정 수정 팝업창에서 수정한 값 저장
    @RequestMapping(value = "/editAccount", method = RequestMethod.POST)
    @ResponseBody
    public String editAccount(AuthVO authVO) {
        int result = authService.updateAccount(authVO);

        if( result != 0 ) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    //기존 계정 비활성화 하기
    @RequestMapping(value = "/deleteAccount", method = RequestMethod.POST)
    @ResponseBody
    public String disableAccount(@RequestBody List<String> checked_arr) {
        AuthVO authVO = new AuthVO();
        authVO.setAccountCheckedList(checked_arr);

        int result = authService.disableAccount(authVO);

        if( result != 0 ) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }

    //기존 계정 활성화 하기
    @RequestMapping(value = "/activeAccount", method = RequestMethod.POST)
    @ResponseBody
    public String enableAccount(@RequestBody List<String> checked_arr) {

        AuthVO authVO = new AuthVO();
        authVO.setAccountCheckedList(checked_arr);

        int result = authService.enableAccount(authVO);

        if( result != 0 ) {
            return "SUCC";
        } else {
            return "FAIL";
        }
    }
}

