package ai.maum.biz.happycall.models.dto;

import lombok.Data;

import java.util.List;

@Data
public class CmContractDTO {
	// 테이블 기본 컬럼
	private String contractNo;
	private String campaignId;
	private String custId;
	private String telNo;
	private String custOpId;
	private String prodId;
	private Integer callTryCount;
	private String lastCallId;
	private String isInbound;
	private String creatorId;
	private String updaterId;
	private String createdDtm;
	private String updatedDtm;

	// 모니터링 조회 결과 컬럼
	private String campaignNm;
	private String custNm;
	private String targetDt;
	private String targetYn;
	private String assignedDt;
	private String assignedYn;
	private String custOpNm;
	private String callDate;
	private String callStatus;
	private String callStatusNm;
	private String mntStatus;
	private String mntStatusName;
	private String callMemo;
	private String callId;
	private List<CallHistoryDTO> callHistDTO;

	// OB 팝업 조회 결과 컬럼
	private String prodNm;
	private String juminNo;

	//최종결과
	private String finalResult;

	// IB 조회 결과 컬럼
	private String loanPrice;
	private String bankAccNum;
	private String bankPaymentDay;
	private String duration;

	//콜백관리
	private String callbackYn;
	private String callbackDt;
	private String custTelNo;
}
