package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.mapper.CallbackMapper;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.CallbackVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CallbackServiceImpl implements CallbackService {

    @Autowired
    CallbackMapper callbackMapper;

    @Override
    public List<CmContractDTO> getCallbackTargetList() {
        return callbackMapper.getCallbackTargetList();
    }

    // callbacklist 조회
    @Override
    public List<CmContractDTO> getCallbackList(CallbackVO callbackVO) {
        return callbackMapper.getCallbackList(callbackVO);
    }

    // paging
    @Override
    public int getResultMntTotalCount(CallbackVO callbackVO) { return callbackMapper.getResultMntTotalCount(callbackVO); }

    // 기존 계약중 체크된 거 콜백 취소할 계약
    @Override
    public int undoCallback(CallbackVO callbackVO) {
        return callbackMapper.undoCallback(callbackVO);
    }

    // 기존 계약중 체크된 거 콜백 지정할 계약
    @Override
    public int doCallback(CallbackVO callbackVO) { return callbackMapper.doCallback(callbackVO); }

    // 콜백 관련 정보 수정
    @Override
    public int doCallbackModifiedRow(CallbackVO callbackVO) {
        return callbackMapper.doCallbackModifiedRow(callbackVO);
    }
}
