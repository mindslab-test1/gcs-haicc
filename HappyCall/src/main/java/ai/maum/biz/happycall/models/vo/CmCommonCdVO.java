package ai.maum.biz.happycall.models.vo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
public class CmCommonCdVO {
	/* 로그인 관련 변수 */
	private String idType;
	private String pwType;

	private String goPage;
}
