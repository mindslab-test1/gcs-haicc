package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CmAuthRoleDTO {
    private int roleId;
    private String roleCd;
    private String roleNm;

    private int creatorId;
    private int updaterId;
    private String createdDtm;
    private String updatedDtm;

}


