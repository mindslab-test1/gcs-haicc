package ai.maum.biz.happycall.controller;

import ai.maum.biz.happycall.common.CustomProperties;
import ai.maum.biz.happycall.common.util.Utils;
import ai.maum.biz.happycall.common.util.VariablesMng;
import ai.maum.biz.happycall.models.dto.*;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import ai.maum.biz.happycall.service.AuthService;
import ai.maum.biz.happycall.service.CommonMonitoringService;
import ai.maum.biz.happycall.service.CommonService;
import ai.maum.biz.happycall.service.InboundMonitoringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
public class DashboardController {

    @Inject
    VariablesMng variablesMng;

    /*  인바운드 모니터링 */
    @RequestMapping(value="/dashboard", method = {RequestMethod.GET, RequestMethod.POST})
    public String dashboard(FrontMntVO frontMntVO, HttpServletRequest req, Model model) {
        model.addAttribute("menuId", variablesMng.getMenuIdString("dashboard"));			// menuId 설정.
        return "/monitoring/dashboard";
    }

    @RequestMapping(value="/dashboard2", method = {RequestMethod.GET, RequestMethod.POST})
    public String dashboard2(FrontMntVO frontMntVO, HttpServletRequest req, Model model) {
        model.addAttribute("menuId", variablesMng.getMenuIdString("dashboard2"));			// menuId 설정.
        return "/monitoring/dashboard2";
    }
}
