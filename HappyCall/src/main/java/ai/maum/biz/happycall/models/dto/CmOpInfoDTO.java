package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CmOpInfoDTO {
	private String custOpId;
	private String custOpNm;
	private String password;
	private String idAddr;
	private String deptCd;
	private String positionCd;
	private String custOpStatus;
	private String useYn;
	private String creatorId;
	private String updaterId;
	private String createdDtm;
	private String updatedDtm;

	private String count;
	private String custOpStatusNm;

}
