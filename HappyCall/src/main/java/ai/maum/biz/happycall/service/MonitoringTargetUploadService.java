package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;

import java.util.List;
import java.util.Map;

public interface MonitoringTargetUploadService {

    int getMonitoringTargetUploadCount(FrontMntVO frontMntVO);

    List<CmContractDTO> getMonitoringTargetUploadList(FrontMntVO frontMntVO);

    List<CmContractDTO> getExcelTmpList();

    int getExcelTmpCount();

    int uploadExcelTmp(List<Map<String, String>> paramMap);

    int resetExcelTmp();

    int uploadTarget(List<CmContractDTO> cmContractDTOList);

    int updateUploadTarget(FrontMntVO frontMntVO);

    int addUploadTarget(FrontMntVO frontMntVO);

}
