package ai.maum.biz.happycall.service;

import java.util.Collection;
import java.util.List;

import ai.maum.biz.happycall.models.dto.CmAuthUserDTO;
import ai.maum.biz.happycall.models.dto.CmAuthRoleDTO;
import ai.maum.biz.happycall.models.vo.AuthVO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public interface AuthService extends UserDetailsService{

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException;

    public Collection<GrantedAuthority> getAuthorities(String username);

//    public CmAuthUserDTO save(CmAuthUserDTO account,String role);

    public List<CmAuthRoleDTO> getRoleList();

    public CmAuthUserDTO getAccount(String userId);

    public int updateAccount(AuthVO authVO);

    public int disableAccount(AuthVO authVO);

    public int enableAccount(AuthVO authVO);

    public List<CmAuthUserDTO> getUserList(FrontMntVO frontMntVO);

    public int getresultUserTotalCount(FrontMntVO frontMntVO);

    public int addAccount(AuthVO authVO);

    public int checkDup(AuthVO authVO);

    public CmAuthUserDTO getAccountById(int id);

    public List<CmAuthRoleDTO> getAuthList(String username);

}
