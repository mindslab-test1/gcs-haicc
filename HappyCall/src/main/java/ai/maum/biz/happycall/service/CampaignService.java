package ai.maum.biz.happycall.service;


import ai.maum.biz.happycall.models.dto.CmCampaignInfoDTO;
import ai.maum.biz.happycall.models.vo.MntTargetMngVO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CampaignService {

    List<CmCampaignInfoDTO> getCampaignTaskList(int campaignId);

    List<CmCampaignInfoDTO> getCampaignListByService(String serviceName);

    int createServiceMonitoring(MntTargetMngVO mntTargetMngVO);


}
