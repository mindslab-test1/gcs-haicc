package ai.maum.biz.happycall.mapper;

import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface MonitoringTargetUploadMapper {
    int getMonitoringTargetUploadCount(FrontMntVO frontMntVO);

    List<CmContractDTO> getMonitoringTargetUploadList(FrontMntVO frontMntVO);

    List<CmContractDTO> getExcelTmpList();

    int getExcelTmpCount();

    int uploadExcelTmp(List<Map<String, String>> paramMap);

    int resetExcelTmp();

    int uploadTarget(List<CmContractDTO> cmContractDTOList);

    int uploadTargetOB(List<CmContractDTO> cmContractDTOList);

    int updateUploadTarget(FrontMntVO frontMntVO);

    int updateUploadTargetOB(FrontMntVO frontMntVO);

    int addUploadTarget(FrontMntVO frontMntVO);

    int addUploadTargetOB(FrontMntVO frontMntVO);

}
