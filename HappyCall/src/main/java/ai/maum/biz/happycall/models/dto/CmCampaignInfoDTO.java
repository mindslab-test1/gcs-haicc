package ai.maum.biz.happycall.models.dto;

import lombok.Data;

@Data
public class CmCampaignInfoDTO {
	private String seq;
	private String campId;				//campaign seq
	private String campNm;				//campaign 이름
	private String category;			//대분류
	private String task;
	private String taskType;
	private String taskAnswer;
	private String taskInfo;			//C(choose) : 양자 선택 V(value) : 값 입력 N(nothing) : 받지 않음
	
	private String taskValue;
	private String serviceName;		//서비스별 캠페인 리스트 조회 변수
}
