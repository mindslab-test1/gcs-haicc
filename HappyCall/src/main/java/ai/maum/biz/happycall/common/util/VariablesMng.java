package ai.maum.biz.happycall.common.util;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
@Data
public class VariablesMng {
    private String callStatusCode = "FCD_02";            //콜상태코드. FCD_02
    private String monitoringResultCode = "FCD_09";        //모니터링결과. FCD_09
    private String finalResultCode = "FCD_10";            //최종결과. FCD_10
    private String custOpInfoCode = "CUST_OP";            //상담사 정보. CUST_OP
    private String mntType = "MNT_TYPE";                //모니터링 종류. MNT_TYPE
    private String prod_name = "PROD_NAME";                //계약 종류. PROD_NAME
    private String callback_status = "FCD_11";            //콜백대상여부. FCD_11

    private int callCount = 30;                        //최종결과 판별용 콜 횟수
    private int basePeriod = 7;                            //최종결과 판별용 기간

    private HashMap<String, String> menuId = new HashMap<String, String>();

    public VariablesMng() {
        menuId.put("dashboard", "m00");            //대시보드
        menuId.put("dashboard2", "m99");            //대시보드
        menuId.put("mntTargetUpload", "m01");            //모니터링 대상 업로드
        menuId.put("mntTartgetMng", "m02");                //모니터링 대상 관리
        menuId.put("opAssign", "m03");                    //상담사 배정
        menuId.put("manualCallMnt", "m04");                //수동 모니터링 실행
        menuId.put("autoCallMnt", "m05");                //자동 모니터링 실행
        menuId.put("mntResult", "m06");                    //모니터링 결과
        menuId.put("callStatus", "m07");                //인바운드 모니터링
        menuId.put("ibMntResult", "m08");                //I/B 모니터링 결과
        menuId.put("userManage", "m09");                //사용자 관리
        menuId.put("callbackManage", "m10");            //콜백 관리
    }

    public String getMenuIdString(String menuUrl) {
        return menuId.get(menuUrl);
    }
}

