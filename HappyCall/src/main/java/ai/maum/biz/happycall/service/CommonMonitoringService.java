package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.models.dto.CallStatusDTO;
import ai.maum.biz.happycall.models.dto.CmCampaignInfoDTO;
import ai.maum.biz.happycall.models.dto.CmCampaignScoreDTO;
import ai.maum.biz.happycall.models.dto.CmSttResultDetailDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommonMonitoringService {
    List<CallStatusDTO> getCountOfCall(FrontMntVO frontMntVO);

    String getRecentContractMemo(FrontMntVO frontMntVO);

    List<CmCampaignInfoDTO> getCallPopMonitoringResultList(FrontMntVO frontMntVO);

    List<CmCampaignScoreDTO> getScoreList(FrontMntVO frontMntVO);

    List<CmSttResultDetailDTO> getSttResultAllList(FrontMntVO frontMntVO);

}
