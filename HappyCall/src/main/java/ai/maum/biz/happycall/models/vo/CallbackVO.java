package ai.maum.biz.happycall.models.vo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Data
public class CallbackVO {

	private String schCampaignId;
	private String schCustNm;
	private String schCustTelNo;
	private String schCallbackStatus;
	private String schCallbackDt;
	private String callId;
	private List<String> callIdList;

	private String modifyingCallbackDt;

	private String pageInitPerPage;
	private String currentPage;
	private int startRow;
	private int lastRow;

	/* 정렬 관련 변수 */
	private String sortingTarget;
	private String direction;

	private TracingVO tracingVO = new TracingVO();
}


