package ai.maum.biz.happycall.service;

import ai.maum.biz.happycall.models.dto.SipAccountDTO;
import ai.maum.biz.happycall.models.dto.CmContractDTO;
import ai.maum.biz.happycall.models.vo.FrontMntVO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InboundMonitoringService {
    List<CmContractDTO> getInboundCallMntList(FrontMntVO frontMntVO);

    int getInboundCallMntCount(FrontMntVO frontMntVO);

    CmContractDTO getInboundCallMntData(FrontMntVO frontMntVO);

    List<SipAccountDTO> getPhoneList(FrontMntVO frontMntVO);

    List<CmContractDTO> getLatestCallList();

    int updateMemo(FrontMntVO frontMntVO);

}
