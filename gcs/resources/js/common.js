// MINDsLab. YMJ. 20190830

$(document).ready(function (){
	$('.btn_potal').on('click',function(){	
		$('#wrap').addClass('nav_show');
	});
    $('.btn_potal_close').on('click',function(){	
        $(this).closest('#wrap').removeClass('nav_show');	
    });

	$('.pnb .nav_pnb li a').on('click',function(){	
        $('.pnb .nav_pnb li a').removeClass('active');
        $(this).addClass('active');
	});       
    
    //profile 설정
	$('#header .sta .ico_profile').on('click',function(){	
        $('.lyrWrap').remove();
        
        //Layer popup 생성 
        $('body').append(' \
            <div class="lyrWrap" tabindex="0" data-tooltip-con="main_srch" data-focus="main_srch" data-focus-prev="main_srch_close"> \
                <div class="lyr_bg"></div> \
                <div class="lyrBox"> \
                    <div class="lyr_top"> \
                        <h3>알림</h3> \
                        <button class="btn_lyr_close">닫기</button> \
                     </div> \
                    <div class="lyr_mid"> \
                        본 화면은 기능개발이 되어 있지 않은 단순 UI만 제공되는 퍼블리싱 파일입니다.<br> \
                        해당 기능 개발 완료 후 common.js에서 profile 설정을 모두 삭제하세요. \
                    </div> \
                    <div class="lyr_btm"> \
                        <div class="btnBox sz_small"> \
                            <button class="btn_lyr_close" data-focus="main_srch_close" data-focus-next="main_srch_close">닫기</button> \
                        </div> \
                    </div> \
                </div> \
            </div>'
        );
        //Layer popup 삭제 
        $('.btn_lyr_close, .lyr_bg').on('click',function(){
            $(this).closest('.lyrWrap').addClass('lyr_hide').delay(500).queue(function() { $(this).remove(); });
        });	
	});    
});	

